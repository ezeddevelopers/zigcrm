import 'package:shared_preferences/shared_preferences.dart';

class LocalStorage {
  void storeToken(String token) async {
    SharedPreferences store = await SharedPreferences.getInstance();
    store.setString('apiToken', token);
  }

  Future<String> getToken() async {
    SharedPreferences store = await SharedPreferences.getInstance();
    return store.getString('apiToken');
  }

  Future<bool> isToken() async {
    SharedPreferences store = await SharedPreferences.getInstance();
    if (store.getString('apiToken') != null)
      return true;
    else
      return false;
  }

  void clearStore() async {
    SharedPreferences store = await SharedPreferences.getInstance();
    store.clear();
  }
}
