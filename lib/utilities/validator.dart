class InputValidator {
  String email(value) {
    Pattern pattern =
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
        r"{0,253}[a-zA-Z0-9])?)*$";
    RegExp regex = new RegExp(pattern);
    if (value.isEmpty) {
      return 'Email is Required';
    } else if (!regex.hasMatch(value) || value == null)
      return 'Enter a valid email address';
    else
      return null;
  }

  String password(value) {
    if (value.isEmpty) {
      return 'Password is Required';
    } else
      return null;
  }

  String notes(value) {
    if (value.isEmpty) {
      return 'Note is Required';
    } else
      return null;
  }

  String name(value) {
    if (value.isEmpty) {
      return 'Name is Required';
    } else
      return null;
  }

  String otp(value) {
    if (value.isEmpty) {
      return 'OTP is Required';
    } else
      return null;
  }

  String phone(value) {
    final RegExp phoneRegex = new RegExp(r'[!@#<>?":_`~;[\]\\|=+)(*&^%0-9-]');
    if (value.isEmpty) {
      return 'Phone Number is Required';
    } else if (!phoneRegex.hasMatch(value)) {
      return 'Please enter valid phone number';
    }
    return null;
  }
}
