import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF2AABD3);
const kSecondaryColor = Color(0xFF00D3C1);
const kLightGrey = Color(0xFFC8CDDC);
const kDarkGreen = Color(0xFF36C90A);

const kPrimaryTextColor = Color(0xFF505050);
const kSecondaryTextColor = Color(0xFFC8CDDC);
const kButtonGreenColor = Color(0xFF009688);

const kPrimaryGradient = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  stops: [0.4, 0.9],
  colors: [kPrimaryColor, kSecondaryColor],
);

const kSecondaryGradient = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  stops: [0.7, 1],
  colors: [kPrimaryColor, kSecondaryColor],
);

const kFontSizeInputField = 13.0;
const kFontSizeMedium = 14.0;
const kFontSizeLarge = 15.0;
const kFontSizeBig = 18.0;
const kFontSizeTitle = 25.0;

class PrimaryClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height - 20);
    var firstEndPoint = new Offset(size.width / 2.25, size.height - 30.0);
    var firstControlPoint = new Offset(size.width / 4, size.height);
    var secondEndPoint = new Offset(size.width, size.height - 30.0);
    var secondControlPoint =
        new Offset(size.width - (size.width / 3.25), size.height - 50);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height - 20);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
