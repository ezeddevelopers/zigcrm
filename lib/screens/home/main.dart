import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/customer.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/screens/home/screens/customer_details.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'components/customer_bottom_sheet.dart';

class HomeApp extends StatefulWidget {
  HomeApp({Key key}) : super(key: key);

  @override
  _HomeAppState createState() => _HomeAppState();
}

class _HomeAppState extends State<HomeApp> {
  @override
  void initState() {
    Provider.of<Store>(context, listen: false).fetchCustomers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListViewer(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: kPrimaryColor,
        child: Icon(Icons.person_add),
        onPressed: () {
          showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              builder: (context) => SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.only(
                          bottom: MediaQuery.of(context).viewInsets.bottom),
                      child: CustomerBottomSheet(),
                    ),
                  ));
        },
      ),
    );
  }
}

class ListViewer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget customersList(data) {
      final customerData = data.customers.data;
      if (data.customers.loading) {
        return Center(
          child: SizedBox(
            child: CircularProgressIndicator(
              strokeWidth: 3.0,
              valueColor: AlwaysStoppedAnimation(kPrimaryColor),
            ),
            height: 30.0,
            width: 30.0,
          ),
        );
      } else {
        if (data.customers.error) {
          return Center(
            child: data.customers.retry
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(data.customers.status),
                      FlatButton(
                        color: kPrimaryColor,
                        onPressed: () {
                          data.customerLoading(true);
                          data.fetchCustomers();
                        },
                        child: Text(
                          'Retry',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )
                : Text(data.customers.status),
          );
        } else if (data.customers.data.length > 0) {
          return ListView.builder(
            itemCount: customerData.length,
            itemBuilder: (context, index) {
              return SingleLead(customerData[index]);
            },
          );
        } else {
          return Center(
            child: data.customers.retry
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(data.customers.status),
                      FlatButton(
                        color: kPrimaryColor,
                        onPressed: () {},
                        child: Text(
                          'Retry',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )
                : Text(data.customers.status),
          );
        }
      }
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Header(),
          Consumer<Store>(
            builder: (context, data, child) {
              return Expanded(
                child: NotificationListener<ScrollNotification>(
                  child: customersList(data),
                  onNotification: (ScrollNotification scrollInfo) {
                    FocusScope.of(context).unfocus();
                    return false;
                  },
                ),
              );
            },
          )
        ],
      ),
    );
  }
}

class Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    const kDefaultPadding = 15.0;
    return Container(
      height: size.height * 0.25,
      child: Stack(
        children: [
          Container(
            height: size.height * 0.25 - 27,
            decoration: BoxDecoration(
              gradient: kPrimaryGradient,
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(36.0),
                bottomRight: Radius.circular(36.0),
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            right: 0,
            left: 0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(kDefaultPadding),
                  child: Text(
                    'SEARCH LEADS',
                    style: TextStyle(
                      fontSize: kFontSizeBig,
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                      letterSpacing: 0.5,
                    ),
                  ),
                ),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: kDefaultPadding),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.3),
                          spreadRadius: 3,
                          blurRadius: 4,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Consumer<Store>(
                      builder: (context, storeData, child) {
                        return TextField(
                          onChanged: (val) => {
                            storeData.fetchCustomers(query: val, loader: false)
                          },
                          decoration: InputDecoration(
                            hintStyle: TextStyle(
                              fontSize: kFontSizeMedium,
                            ),
                            hintText: 'SEARCH  LEADS',
                            suffixIcon: Icon(Icons.search),
                            border: InputBorder.none,
                            contentPadding: EdgeInsets.all(20),
                          ),
                        );
                      },
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class SingleLead extends StatelessWidget {
  final Customer customer;
  SingleLead(this.customer);
  @override
  Widget build(BuildContext context) {
    DateTime dateTime = customer.registrationDate;
    String timeMode;
    int timeHour;
    dynamic timeMinute;
    if (dateTime.hour > 12) {
      timeHour = dateTime.hour - 12;
      timeMode = 'PM';
    } else {
      timeHour = dateTime.hour;
      timeMode = 'AM';
    }
    if (dateTime.minute > 9) {
      timeMinute = dateTime.minute;
    } else {
      timeMinute = '0${dateTime.minute.toString()}';
    }
    return Padding(
      padding: const EdgeInsets.only(
        left: 15.0,
        right: 15.0,
        bottom: 15.0,
      ),
      child: FlatButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return CustomerDetails(customer: customer);
          }));
        },
        padding: EdgeInsets.all(0.0),
        child: Row(
          children: [
            CircleAvatar(
              backgroundColor: kLightGrey,
              radius: 28.0,
              child: Center(
                child: Icon(
                  Icons.person,
                  color: Colors.white,
                  size: 32.0,
                ),
              ),
            ),
            SizedBox(
              width: 20.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  customer.customerName,
                  style: TextStyle(
                      color: kSecondaryColor,
                      fontSize: kFontSizeInputField,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Text(
                  customer.customerStage != null ? customer.customerStage : '',
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                    color: Colors.black54,
                  ),
                ),
              ],
            ),
            Spacer(),
            Text(
              '$timeHour.$timeMinute $timeMode',
              style: TextStyle(
                fontSize: 10.0,
                color: Colors.black54,
                fontWeight: FontWeight.w400,
              ),
            )
          ],
        ),
      ),
    );
  }
}
