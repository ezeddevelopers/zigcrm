import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:zigcrm/modal/customer.dart';

import 'package:zigcrm/screens/home/screens/message_template_screen.dart';
import 'package:zigcrm/screens/home/screens/notes_screen.dart';
import 'package:zigcrm/screens/home/screens/call_log_screen.dart';
import 'package:zigcrm/screens/home/screens/sms_screen.dart';
import 'package:zigcrm/screens/home/screens/task_screen.dart';
import 'package:zigcrm/screens/home/screens/external_log_screen.dart';

class IconNavigator extends StatelessWidget {
  final Customer customer;
  IconNavigator(this.customer);
  @override
  Widget build(BuildContext context) {
    toastMsg(msg) {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: Colors.black54,
          textColor: Colors.white,
          fontSize: 12.0);
    }

    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 40.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              child: FaIcon(FontAwesomeIcons.whatsapp),
              onTap: () {
                if (customer.customerWhatsapp != null) {
                  try {
                    int number = int.parse(customer.customerWhatsapp);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return MessageTemplateScreen(
                        template: 'whatsapp',
                        senderInfo: number.toString(),
                        customerId: customer.customerId,
                      );
                    }));
                  } catch (err) {
                    toastMsg("whatsapp Number is not valid type");
                  }
                } else {
                  toastMsg("whatsapp Number Not Found");
                }
              },
            ),
            GestureDetector(
              child: Icon(Icons.rate_review),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return NotesScreen(customer.customerId);
                }));
              },
            ),
            GestureDetector(
              child: Icon(Icons.email),
              onTap: () {
                if (customer.customerEmail != null) {
                  Navigator.push(context, MaterialPageRoute(builder: (context) {
                    return MessageTemplateScreen(
                        template: 'email',
                        senderInfo: customer.customerEmail,
                        customerId: customer.customerId);
                  }));
                } else {
                  toastMsg("whatsapp Number Not Found");
                }
              },
            ),
            GestureDetector(
              child: Icon(Icons.phone_in_talk),
              onTap: () {
                showAlertDialog(context);
              },
            ),
            GestureDetector(
              child: Icon(Icons.messenger),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return SMSScreen(
                    customer: customer,
                  );
                }));
              },
            ),
            GestureDetector(
              child: Icon(Icons.enhanced_encryption),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return TaskScreen(customer.customerId);
                }));
              },
            ),
            GestureDetector(
              child: Icon(Icons.import_export),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return ExternalLogs(customer.customerId);
                }));
              },
            ),
            GestureDetector(
              child: FaIcon(FontAwesomeIcons.telegram),
              onTap: () {
                if (customer.customerTelegram != null) {
                  try {
                    int number = int.parse(customer.customerTelegram);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return MessageTemplateScreen(
                        template: 'telegram',
                        senderInfo: number.toString(),
                        customerId: customer.customerId,
                      );
                    }));
                  } catch (err) {
                    toastMsg("Telegram Username is not valid type");
                  }
                } else {
                  toastMsg("Telegram Username Not Found");
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed: () async {
        var url = "tel:${customer.customerPhone}";
        if (await canLaunch(url)) {
          await launch(url);
          Navigator.pop(context);
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return CallLog(customer.customerId);
          }));
        }
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text("Confirmation"),
      content: Text("Do you really want to continue with call"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
