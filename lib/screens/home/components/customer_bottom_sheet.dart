import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/customer.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/screens/home/screens/customer_details.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';
import 'package:zigcrm/utilities/validator.dart';

class CustomerBottomSheet extends StatefulWidget {
  final String type;
  final String name;
  final String email;
  final String phone;
  final int customerId;
  final Customer customer; // 'add' & 'edit' two types are there!
  CustomerBottomSheet({
    this.type = 'add',
    this.name,
    this.email,
    this.phone,
    this.customerId,
    this.customer,
  });

  @override
  _CustomerBottomSheetState createState() => _CustomerBottomSheetState();
}

class _CustomerBottomSheetState extends State<CustomerBottomSheet> {
  String name = '';
  String nameError = '';
  String email = '';
  String emailError = '';
  String phone = '';
  String phoneError = '';
  bool loader = false;

  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {
      _controllerName.text = widget.name;
      _controllerEmail.text = widget.email;
      _controllerPhone.text = widget.phone;
      name = widget.name;
      email = widget.email;
      phone = widget.phone;
    });
  }

  @override
  Widget build(BuildContext context) {
    final inputNode = FocusScope.of(context);
    return Container(
      color: Color(0xff757575),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    widget.type == 'add' ? 'CREATE LEAD' : 'EDIT LEAD',
                    style: TextStyle(
                      fontSize: kFontSizeLarge,
                      color: kLightGrey,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
                child: TextField(
                  controller: _controllerName,
                  onEditingComplete: () {
                    inputNode.nextFocus();
                  },
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  decoration: InputDecoration(
                    hintText: 'FULL NAME',
                    suffixIcon: Icon(Icons.person),
                  ),
                  onChanged: (val) {
                    name = val;
                  },
                  // validator: (value) => InputValidator().name(value),
                  // onSaved: (val) => name = val,
                ),
              ),
              Text(
                nameError,
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 12.0,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
                child: TextField(
                  controller: _controllerEmail,
                  onEditingComplete: () {
                    inputNode.nextFocus();
                  },
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  decoration: InputDecoration(
                    hintText: 'EMAIL',
                    suffixIcon: Icon(Icons.email),
                    focusColor: kButtonGreenColor,
                  ),
                  onChanged: (val) {
                    email = val;
                  },
                ),
              ),
              Text(
                emailError != null ? emailError : '',
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 12.0,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
                child: TextField(
                  controller: _controllerPhone,
                  keyboardType: TextInputType.number,
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  decoration: InputDecoration(
                    hintText: 'PHONE NUMBER',
                    suffixIcon: Icon(Icons.phone),
                  ),
                  onChanged: (val) {
                    phone = val;
                  },
                ),
              ),
              Text(
                phoneError != null ? phoneError : '',
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 12.0,
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Align(
                alignment: Alignment.center,
                child: widget.type == 'add'
                    ? FlatButton(
                        onPressed: () async {
                          void success(String msg) {
                            setState(() {
                              loader = false;
                            });
                            Fluttertoast.showToast(
                              msg: msg,
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black54,
                              textColor: Colors.white,
                              fontSize: 12.0,
                            );
                            Navigator.pop(context);
                          }

                          void error(String msg) {
                            setState(() {
                              loader = false;
                              phoneError = msg;
                            });
                          }

                          final nameValidation = InputValidator().name(name);
                          final emailValidation = InputValidator().email(email);
                          final phoneValidation = InputValidator().phone(phone);

                          if (nameValidation != null)
                            setState(() {
                              nameError = emailValidation;
                            });
                          else
                            setState(() {
                              nameError = '';
                            });

                          if (emailValidation != null)
                            setState(() {
                              emailError = emailValidation;
                            });
                          else
                            setState(() {
                              emailError = '';
                            });

                          if (phoneValidation != null)
                            setState(() {
                              phoneError = phoneValidation;
                            });
                          else
                            setState(() {
                              phoneError = '';
                            });

                          if (nameValidation == null &&
                              emailValidation == null &&
                              phoneValidation == null) {
                            setState(() {
                              loader = true;
                            });
                            final token = await LocalStorage().getToken();
                            final data = CreateCustomer(
                              apiToken: token,
                              name: name,
                              email: email,
                              phone: int.parse(phone),
                            );
                            Provider.of<Store>(context, listen: false)
                                .createCustomer(data, success, error);
                          }
                        },
                        textColor: Colors.white,
                        color: kButtonGreenColor,
                        padding: EdgeInsets.symmetric(
                          horizontal: 50.0,
                          vertical: 15.0,
                        ),
                        child: loader
                            ? SizedBox(
                                child: CircularProgressIndicator(
                                  strokeWidth: 2.0,
                                  valueColor:
                                      AlwaysStoppedAnimation(Colors.white),
                                ),
                                height: 20.0,
                                width: 20.0,
                              )
                            : Text('SAVE LEAD'),
                      )
                    : FlatButton(
                        onPressed: () async {
                          void success(String msg) {
                            setState(() {
                              loader = false;
                            });
                            Fluttertoast.showToast(
                              msg: msg,
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIosWeb: 1,
                              backgroundColor: Colors.black54,
                              textColor: Colors.white,
                              fontSize: 12.0,
                            );
                            final updateData = widget.customer;
                            updateData.customerName = name;
                            updateData.customerEmail = email;
                            updateData.customerPhone = phone;
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return CustomerDetails(customer: widget.customer);
                            }));
                          }

                          void error(String msg) {
                            setState(() {
                              loader = false;
                              phoneError = msg;
                            });
                          }

                          final nameValidation = InputValidator().name(name);
                          final emailValidation = InputValidator().email(email);
                          final phoneValidation = InputValidator().phone(phone);

                          if (nameValidation != null)
                            setState(() {
                              nameError = emailValidation;
                            });
                          else
                            setState(() {
                              nameError = '';
                            });

                          if (emailValidation != null)
                            setState(() {
                              emailError = emailValidation;
                            });
                          else
                            setState(() {
                              emailError = '';
                            });

                          if (phoneValidation != null)
                            setState(() {
                              phoneError = phoneValidation;
                            });
                          else
                            setState(() {
                              phoneError = '';
                            });

                          if (nameValidation == null &&
                              emailValidation == null &&
                              phoneValidation == null) {
                            setState(() {
                              loader = true;
                            });

                            if (name != widget.name ||
                                email != widget.email ||
                                phone != widget.phone) {
                              final token = await LocalStorage().getToken();
                              final data = CreateCustomer(
                                apiToken: token,
                                name: name,
                                email: email,
                                phone: int.parse(phone),
                              );
                              Provider.of<Store>(context, listen: false)
                                  .updateCustomer(
                                      data,
                                      widget.customerId.toString(),
                                      success,
                                      error);
                            } else {
                              setState(() {
                                phoneError =
                                    'You can\'t update without any change.';
                              });
                            }
                          }
                        },
                        textColor: Colors.white,
                        color: kButtonGreenColor,
                        padding: EdgeInsets.symmetric(
                          horizontal: 50.0,
                          vertical: 15.0,
                        ),
                        child: loader
                            ? SizedBox(
                                child: CircularProgressIndicator(
                                  strokeWidth: 2.0,
                                  valueColor:
                                      AlwaysStoppedAnimation(Colors.white),
                                ),
                                height: 20.0,
                                width: 20.0,
                              )
                            : Text(
                                'EDIT LEAD',
                                style: TextStyle(
                                  fontSize: kFontSizeLarge,
                                ),
                              ),
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
