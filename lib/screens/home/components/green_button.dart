import 'package:flutter/material.dart';
import 'package:zigcrm/utilities/constants.dart';

class GreenButton extends StatelessWidget {
  final String label;
  final Function onPress;
  final bool loader;
  GreenButton({this.label, this.onPress, this.loader = false});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: FlatButton(
          padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 18.0),
          onPressed: onPress,
          color: kButtonGreenColor,
          child: loader
              ? SizedBox(
                  child: CircularProgressIndicator(
                    strokeWidth: 2.0,
                    valueColor: AlwaysStoppedAnimation(Colors.white),
                  ),
                  height: 20.0,
                  width: 20.0,
                )
              : Text(
                  '$label',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                )),
    );
  }
}
