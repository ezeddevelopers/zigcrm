import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zigcrm/utilities/constants.dart';

class HeaderRoundImage extends StatelessWidget {
  final String networkImage;
  HeaderRoundImage({this.networkImage});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return CircleAvatar(
      backgroundImage: NetworkImage(networkImage),
      backgroundColor: kLightGrey,
      radius: size.height / 14,
    );
  }
}

class HeaderRoundIcon extends StatelessWidget {
  final IconData icon;
  final String iconType;
  HeaderRoundIcon({this.icon, this.iconType});
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(bottom: 10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
            blurRadius: 10,
            color: Colors.black12,
            spreadRadius: 1,
          )
        ],
      ),
      child: CircleAvatar(
        backgroundColor: Colors.white,
        radius: size.height / 14,
        child: iconType != 'fa'
            ? Icon(
                icon,
                size: size.height / 15,
                color: Colors.black,
              )
            : FaIcon(
                icon,
                size: size.height / 15,
                color: Colors.black,
              ),
      ),
    );
  }
}
