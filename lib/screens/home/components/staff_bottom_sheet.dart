import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/staff.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';

class AssignMultipleStaffs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff757575),
      child: Container(
        height: 400.0,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(25.0),
              child: Text(
                'ASSIGN EMPLOYEE',
                style: TextStyle(
                    fontSize: 20.0,
                    color: kLightGrey,
                    fontWeight: FontWeight.w400),
              ),
            ),
            Consumer<Store>(
              builder: (context, data, child) {
                final itemData = data.staffs;
                if (itemData.loading) {
                  return Container(
                    height: 50.0,
                    child: Center(
                      child: SizedBox(
                        child: CircularProgressIndicator(
                          strokeWidth: 2.0,
                          valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                        ),
                        height: 20.0,
                        width: 20.0,
                      ),
                    ),
                  );
                } else if (itemData.error) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(itemData.status),
                      FlatButton(
                        color: kPrimaryColor,
                        onPressed: () {
                          data.staffsLoading(true);
                          data.fetchStaff();
                        },
                        child: Text(
                          'Retry',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  );
                } else
                  return Expanded(
                    child: ListView.builder(
                        itemCount: itemData.data.length,
                        itemBuilder: (context, index) {
                          final singleItem = itemData.data[index];
                          return Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 20.0),
                            child: ListTile(
                              title: Text(
                                singleItem.userName,
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w400),
                              ),
                              trailing: Checkbox(
                                value: data.selectedStaff.contains(singleItem)
                                    ? true
                                    : false,
                                onChanged: (val) {
                                  data.selectStaffToList(singleItem);
                                },
                              ),
                            ),
                          );
                        }),
                  );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class SingleRoundedStaff extends StatelessWidget {
  final Staff staff;
  SingleRoundedStaff(this.staff);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 10.0),
      child: Stack(
        children: [
          CircleAvatar(
            backgroundColor: kSecondaryColor,
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    staff.userName,
                    style: TextStyle(
                      fontSize: 8.0,
                      color: Colors.white,
                    ),
                    textAlign: TextAlign.center,
                  )
                  // Text('Assing')
                ],
              ),
            ),
            radius: 30.0,
          ),
          Positioned(
            right: 0,
            child: GestureDetector(
              onTap: () {
                Provider.of<Store>(context, listen: false)
                    .selectStaffToList(staff);
              },
              child: Container(
                height: 20.0,
                width: 20.0,
                decoration: BoxDecoration(
                  color: Colors.black54,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Center(
                  child: IconButton(
                    padding: EdgeInsets.zero,
                    onPressed: null,
                    icon: Icon(
                      Icons.close,
                      size: 10.0,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
