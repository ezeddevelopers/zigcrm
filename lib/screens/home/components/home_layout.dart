import 'package:flutter/material.dart';
import 'package:zigcrm/utilities/constants.dart';

class HomeLayout extends StatelessWidget {
  final String headerTitle;
  final Widget headerItem;
  final Widget headerAction;
  final Widget body;
  final bool scrollable;
  final Widget list;
  final bool backNavigatorStatus;
  final Function backNavigator;

  HomeLayout({
    this.headerTitle,
    this.headerItem,
    this.headerAction,
    this.body,
    this.scrollable = true,
    this.list,
    this.backNavigatorStatus = false,
    this.backNavigator,
  });

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    // *************************************************************************
    //                FUNCTION TO RENDER BODY OF THIS LAYOUT START
    // *************************************************************************

    Widget renderBody() {
      return (Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            color: Colors.white,
            height: size.height * 0.2,
            child: Stack(
              children: [
                ClipPath(
                  clipper: PrimaryClipper(),
                  child: Container(
                    height: size.height * 0.2 - size.height / 30,
                    color: kPrimaryColor,
                  ),
                ),
                Positioned(
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Align(
                    alignment: Alignment.center,
                    child: headerItem,
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: body,
          ),
          list != null
              ? (Expanded(
                  child: list,
                ))
              : (Container())
        ],
      ));
    }

    // *************************************************************************
    //                FUNCTION TO RENDER BODY OF THIS LAYOUT ENDS
    // *************************************************************************

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: kPrimaryColor,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            if (backNavigatorStatus) {
              backNavigator();
            } else {
              Navigator.pop(context);
            }
          },
        ),
        actions: headerAction != null ? [headerAction] : [],
        title: Text(
          headerTitle.toUpperCase(),
          style: TextStyle(
            fontWeight: FontWeight.w300,
            letterSpacing: 0.5,
            fontSize: kFontSizeBig,
          ),
        ),
      ),
      body: SafeArea(
          child: scrollable
              ? NotificationListener(
                  onNotification: (ScrollNotification scrollInfo) {
                    // FocusScope.of(context).unfocus();
                    return false;
                  },
                  child: (SingleChildScrollView(
                    child: renderBody(),
                  )),
                )
              : (renderBody())),
    );
  }
}
