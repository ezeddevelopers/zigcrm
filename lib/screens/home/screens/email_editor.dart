import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:html_editor/html_editor.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/template.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/local_storage.dart';

// class EmailEditor extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Editor'),
//       ),
//       body: EmailEditor(title: 'Demo Flutter HTML Editor'),
//     );
//   }
// }

class EmailEditor extends StatefulWidget {
  final String template;
  final int customerId;
  EmailEditor({this.template, this.customerId});

  @override
  _EmailEditorState createState() => _EmailEditorState();
}

class _EmailEditorState extends State<EmailEditor> {
  GlobalKey<HtmlEditorState> keyEditor = GlobalKey();
  String result = "";
  String emailBody = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Email Editor'),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  onChanged: (txt) {
                    setState(() {
                      emailBody = txt;
                    });
                  },
                  decoration: InputDecoration(
                      hintText: 'Email Subject', labelText: 'Email Subject'),
                ),
              ),
              HtmlEditor(
                hint: "Email Body",
                value: widget.template,
                key: keyEditor,
                height: 400,
                showBottomToolbar: false,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FlatButton(
                      color: Colors.blueGrey,
                      onPressed: () {
                        setState(() {
                          keyEditor.currentState.setEmpty();
                        });
                      },
                      child:
                          Text("Reset", style: TextStyle(color: Colors.white)),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    FlatButton(
                      color: Colors.blue,
                      onPressed: () async {
                        final txt = await keyEditor.currentState.getText();
                        // setState(() {
                        //   result = txt;
                        // });
                        if (txt.isNotEmpty) {
                          void success(String msg) {
                            Fluttertoast.showToast(
                                msg: msg,
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black54,
                                textColor: Colors.white,
                                fontSize: 12.0);
                            Navigator.pop(context);
                          }

                          void error(String msg) {
                            Fluttertoast.showToast(
                                msg: msg,
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black54,
                                textColor: Colors.white,
                                fontSize: 12.0);
                          }

                          final token = await LocalStorage().getToken();
                          final data = SendEmailModal(
                            token: token,
                            customerId: widget.customerId,
                            emailSub: emailBody,
                            emailBody: txt,
                          );
                          Provider.of<Store>(context, listen: false)
                              .sendEmail(data, success, error);
                        } else {
                          Fluttertoast.showToast(
                            msg: 'Email Body is empty',
                            toastLength: Toast.LENGTH_SHORT,
                            gravity: ToastGravity.BOTTOM,
                            timeInSecForIosWeb: 1,
                            backgroundColor: Colors.black54,
                            textColor: Colors.white,
                            fontSize: 12.0,
                          );
                        }
                      },
                      child: Text(
                        "Send",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(result),
              )
            ],
          ),
        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
