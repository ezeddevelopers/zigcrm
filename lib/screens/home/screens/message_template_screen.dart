import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zigcrm/modal/logs.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/screens/home/components/home_layout.dart';
import 'package:zigcrm/screens/home/components/header_round_item.dart';
import 'package:zigcrm/screens/home/screens/email_editor.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:clipboard/clipboard.dart';
import 'package:zigcrm/utilities/local_storage.dart';

class MessageTemplateScreen extends StatelessWidget {
  final String template;
  final String senderInfo;
  final int customerId;
  MessageTemplateScreen({this.template, this.senderInfo, this.customerId});

  Widget headerIcon() {
    if (template == 'whatsapp') {
      return HeaderRoundIcon(
        icon: FontAwesomeIcons.whatsapp,
        iconType: 'fa',
      );
    } else if (template == 'email') {
      return HeaderRoundIcon(
        icon: Icons.email,
      );
    } else if (template == 'telegram') {
      return HeaderRoundIcon(
        icon: FontAwesomeIcons.telegram,
        iconType: 'fa',
      );
    } else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<Store>(context, listen: false).fetchTemplates(
        template == 'whatsapp' ? 'whatsapp_templates' : 'email_templates');
    return HomeLayout(
      headerTitle: 'SELECT TEMPLATE',
      headerItem: headerIcon(),
      body: Container(),
      scrollable: false,
      list: ListViewer(template, senderInfo, customerId),
    );
  }
}

class ListViewer extends StatelessWidget {
  final String template;
  final String senderInfo;
  final int customerId;
  ListViewer(this.template, this.senderInfo, this.customerId);

  Widget okButton(onPress) {
    return FlatButton(
      child: Text("OK"),
      onPressed: onPress,
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget templateList(data, customerId) {
      final templateData =
          template == 'email' ? data.templateEmail : data.templateWhatsapp;
      if (templateData.loading) {
        return Container(
          height: 50.0,
          child: Center(
            child: SizedBox(
              child: CircularProgressIndicator(
                strokeWidth: 3.0,
                valueColor: AlwaysStoppedAnimation(kPrimaryColor),
              ),
              height: 30.0,
              width: 30.0,
            ),
          ),
        );
      } else {
        if (templateData.error) {
          return Center(
            child: templateData.retry
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(templateData.status),
                      FlatButton(
                        color: kPrimaryColor,
                        onPressed: () {
                          if (template == 'c') {
                            data.templateWhatsappLoading(true);
                            data.fetchTemplates('whatsapp_templates');
                          }
                          if (template == 'email') {
                            data.templateEmailLoading(true);
                            data.fetchTemplates('email_templates');
                          }
                        },
                        child: Text(
                          'Retry',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )
                : Text(
                    templateData.status,
                  ),
          );
        } else if (templateData.data.length > 0) {
          return ListView.builder(
            itemCount: templateData.data.length,
            itemBuilder: (context, index) {
              final mainData = templateData.data[index];
              return Container(
                margin: EdgeInsets.only(top: 15),
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              mainData.templateName,
                              style: TextStyle(
                                fontSize: 17.0,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 10.0),
                              child: Text(
                                mainData.rawTemplateBody,
                                maxLines: 3,
                                textAlign: TextAlign.justify,
                                style: TextStyle(
                                    fontSize: kFontSizeLarge,
                                    color: Colors.black54),
                              ),
                            ),
                          ],
                        ),
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.messenger,
                        ),
                        onPressed: () async {
                          if (template == 'whatsapp') {
                            var url =
                                "https://wa.me/$senderInfo?text=${mainData.templateBody}";

                            void onPress() async {
                              if (await canLaunch(url)) {
                                await launch(url);
                                Navigator.pop(context);

                                void success(String msg) {
                                  Fluttertoast.showToast(
                                      msg: msg,
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.black54,
                                      textColor: Colors.white,
                                      fontSize: 12.0);
                                }

                                void error(String msg) {
                                  Fluttertoast.showToast(
                                      msg: msg,
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.black54,
                                      textColor: Colors.white,
                                      fontSize: 12.0);
                                }

                                final token = await LocalStorage().getToken();
                                final data = ExternalLog(
                                  apiToken: token,
                                  customerId: customerId,
                                  logText: mainData.templateBody,
                                  logType: 'whatsapp',
                                );

                                Provider.of<Store>(context, listen: false)
                                    .createExternalLog(data, success, error);
                              } else {
                                throw 'Could not launch $url';
                              }
                            }

                            showAlertDialog(context, onPress, 'Whatsapp');
                          } else if (template == 'telegram') {
                            var url = "https://telegram.me/$senderInfo?start";

                            void onPress() {
                              FlutterClipboard.copy(mainData.templateBody)
                                  .then((value) async {
                                if (await canLaunch(url)) {
                                  await launch(url);
                                  Navigator.pop(context);
                                  void success(String msg) {
                                    Fluttertoast.showToast(
                                        msg: msg,
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.black54,
                                        textColor: Colors.white,
                                        fontSize: 12.0);
                                  }

                                  void error(String msg) {
                                    Fluttertoast.showToast(
                                        msg: msg,
                                        toastLength: Toast.LENGTH_SHORT,
                                        gravity: ToastGravity.BOTTOM,
                                        timeInSecForIosWeb: 1,
                                        backgroundColor: Colors.black54,
                                        textColor: Colors.white,
                                        fontSize: 12.0);
                                  }

                                  final token = await LocalStorage().getToken();
                                  final data = ExternalLog(
                                    apiToken: token,
                                    customerId: customerId,
                                    logText: mainData.templateBody,
                                    logType: 'tgram',
                                  );

                                  Provider.of<Store>(context, listen: false)
                                      .createExternalLog(data, success, error);
                                } else {
                                  throw 'Could not launch $url';
                                }
                              });
                            }

                            showAlertDialog(context, onPress, 'Whatsapp');
                          } else if (template == 'email') {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return EmailEditor(
                                template: mainData.templateBody,
                                customerId: customerId,
                              );
                            }));
                          }
                        },
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        } else {
          return Center(
            child: templateData.retry
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(templateData.status),
                      FlatButton(
                        color: kPrimaryColor,
                        onPressed: () {
                          // templateDataLoading(true);
                          // data.fetchCustomerActivity(customerId);
                        },
                        child: Text(
                          'Retry',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )
                : Text(templateData.status),
          );
        }
      }
    }

    return Consumer<Store>(builder: (context, data, child) {
      return templateList(data, customerId);
    });
  }

  showAlertDialog(BuildContext context, Function onPress, String type) {
    Widget cancelButton = FlatButton(
      child: Text("Cancel"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    Widget continueButton = FlatButton(
      child: Text("Continue"),
      onPressed: onPress,
    );

    AlertDialog alert = AlertDialog(
      title: Text("Confirmation"),
      content: Text("Do you really want to continue with $type"),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
