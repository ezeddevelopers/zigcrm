import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/note.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/screens/home/components/home_layout.dart';
import 'package:zigcrm/screens/home/components/header_round_item.dart';
import 'package:zigcrm/screens/home/components/staff_bottom_sheet.dart';
import 'package:zigcrm/screens/home/components/green_button.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';
import 'package:zigcrm/utilities/validator.dart';

class NotesScreen extends StatelessWidget {
  final int customerId;
  NotesScreen(this.customerId);
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    String noteVal;
    return HomeLayout(
      headerTitle: 'CREATE NOTE',
      headerItem: HeaderRoundIcon(
        icon: Icons.person_outline,
      ),
      body: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: TextFormField(
                keyboardType: TextInputType.multiline,
                minLines: 1, //Normal textInputField will be displayed
                maxLines: 4,
                style: TextStyle(
                  fontSize: kFontSizeInputField,
                ),
                onChanged: (val) {
                  noteVal = val;
                },
                validator: (value) => InputValidator().notes(value),
                onSaved: (val) => noteVal = val,
                decoration: InputDecoration(
                    hintText: 'Add notes..',
                    hintStyle: TextStyle(
                      color: Colors.black,
                      fontSize: kFontSizeLarge,
                    ),
                    labelText: 'Add Notes',
                    labelStyle: TextStyle(
                      fontSize: kFontSizeLarge + 2,
                      fontWeight: FontWeight.w500,
                      color: kPrimaryColor,
                    )),
              ),
            ),
            Container(
              height: 70.0,
              child: Staffs(),
            ),
            Consumer<Store>(
              builder: (context, data, child) {
                final itemData = data.selectedStaff;
                return GreenButton(
                  label: 'SAVE NOTE',
                  onPress: () async {
                    void success(String msg) {
                      data.clearSelectedStaffList();
                      Fluttertoast.showToast(
                          msg: msg,
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.black54,
                          textColor: Colors.white,
                          fontSize: 12.0);
                      Navigator.pop(context);
                    }

                    void error(String msg) {
                      Fluttertoast.showToast(
                          msg: msg,
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.black54,
                          textColor: Colors.white,
                          fontSize: 12.0);
                    }

                    if (_formKey.currentState.validate()) {
                      final token = await LocalStorage().getToken();
                      List mentions = [];
                      int i;
                      for (i = 0; i < itemData.length; i++) {
                        mentions.add(itemData[i].userId);
                      }
                      final data = NoteRequest(
                          token: token,
                          note: noteVal,
                          customerId: customerId,
                          mentions: mentions);
                      Provider.of<Store>(context, listen: false)
                          .createNote(data, success, error);
                    }
                  },
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

class Staffs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Provider.of<Store>(context, listen: false).fetchStaff();
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 10.0),
          child: Stack(
            children: [
              GestureDetector(
                child: CircleAvatar(
                  backgroundColor: kPrimaryColor,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.person_add,
                        size: 28.0,
                        color: Colors.white,
                      ),
                      Text(
                        'ASSIGN',
                        style: TextStyle(
                          fontSize: 8.0,
                          color: Colors.white,
                        ),
                        textAlign: TextAlign.center,
                      )
                    ],
                  ),
                  radius: 30.0,
                ),
                onTap: () {
                  FocusScope.of(context).unfocus();
                  showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) => SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: AssignMultipleStaffs(),
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
        Consumer<Store>(
          builder: (context, data, child) {
            final itemData = data.selectedStaff;
            return Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: itemData.length,
                  itemBuilder: (context, index) {
                    return SingleRoundedStaff(itemData[index]);
                  },
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
