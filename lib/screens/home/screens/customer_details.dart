import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/customer.dart';
import 'package:zigcrm/modal/customer_activity.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/screens/home/components/home_layout.dart';
import 'package:zigcrm/screens/home/components/header_round_item.dart';
import 'package:zigcrm/screens/home/components/customer_bottom_sheet.dart';
import 'package:zigcrm/screens/home/components/home_icon_navigator.dart';
import 'package:zigcrm/utilities/constants.dart';

const imageUrl =
    'https://pixinvent.com/materialize-material-design-admin-template/app-assets/images/user/12.jpg';

class CustomerDetails extends StatelessWidget {
  final Customer customer;
  CustomerDetails({this.customer});
  @override
  Widget build(BuildContext context) {
    // Provider.of<Store>(context, listen: false)
    //     .fetchCustomerActivity(customer.customerId);
    Provider.of<Store>(context, listen: false)
        .fetchCustomerActivity(customer.customerId);
    return HomeLayout(
      headerTitle: 'PROFILE & TIMELINE',
      headerItem: HeaderRoundIcon(
        icon: Icons.person,
      ),
      backNavigatorStatus: true,
      backNavigator: () {
        Navigator.pushNamedAndRemoveUntil(
            context, '/appStack', (route) => false);
      },
      scrollable: false,
      headerAction: Padding(
        padding: const EdgeInsets.all(8.0),
        child: IconButton(
          icon: Icon(Icons.settings),
          onPressed: () {
            showModalBottomSheet(
                context: context,
                isScrollControlled: true,
                builder: (context) => SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.only(
                            bottom: MediaQuery.of(context).viewInsets.bottom),
                        child: CustomerBottomSheet(
                          type: 'edit',
                          name: customer.customerName,
                          email: customer.customerEmail,
                          phone: customer.customerPhone,
                          customerId: customer.customerId,
                          customer: customer,
                        ),
                      ),
                    )
                // builder: (context) => AddCustomer(),
                );
          },
        ),
      ),
      body: Column(
        children: [
          IconNavigator(customer),
          Text(
            customer.customerName,
            style: TextStyle(
              fontSize: 22.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            customer.customerStage != null ? customer.customerStage : '',
            style: TextStyle(
              fontSize: 10.0,
              color: Colors.black54,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
        ],
      ),
      list: ListViewer(customer.customerId),
    );
  }
}

class ListViewer extends StatelessWidget {
  final int customerId;
  ListViewer(this.customerId);
  @override
  Widget build(BuildContext context) {
    Widget activityList(data) {
      final activityData = data.customerActivity.data;
      if (data.customerActivity.loading) {
        return Container(
          height: 50.0,
          child: Center(
            child: SizedBox(
              child: CircularProgressIndicator(
                strokeWidth: 3.0,
                valueColor: AlwaysStoppedAnimation(kPrimaryColor),
              ),
              height: 30.0,
              width: 30.0,
            ),
          ),
        );
      } else {
        if (data.customerActivity.error) {
          return Center(
            child: data.customerActivity.retry
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(data.customerActivity.status),
                      FlatButton(
                        color: kPrimaryColor,
                        onPressed: () {
                          data.customerActivityLoading(true);
                          data.fetchCustomerActivity(customerId);
                        },
                        child: Text(
                          'Retry',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )
                : Text(data.customerActivity.status),
          );
        } else if (activityData.length > 0) {
          return ListView.builder(
            itemCount: activityData.length,
            itemBuilder: (context, index) {
              return SingleLog(activityData[index]);
            },
          );
        } else {
          return Center(
            child: data.customerActivity.retry
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(data.customerActivity.status),
                      FlatButton(
                        color: kPrimaryColor,
                        onPressed: () {
                          data.customerActivityLoading(true);
                          data.fetchCustomerActivity(customerId);
                        },
                        child: Text(
                          'Retry',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  )
                : Text(data.customerActivity.status),
          );
        }
      }
    }

    return Container(
      child: Consumer<Store>(
        builder: (context, data, child) {
          return activityList(data);
        },
      ),
    );
  }
}

class SingleLog extends StatelessWidget {
  final CustomerActivity activity;
  SingleLog(this.activity);
  @override
  Widget build(BuildContext context) {
    IconData setIcon(String type) {
      if (type == 'notes') return Icons.rate_review;
      if (type == 'tasks') return Icons.enhanced_encryption;
      if (type == 'customer') return Icons.person_add_alt;
      if (type == 'email') return Icons.email;
      if (type == 'sms') return Icons.messenger;
      if (type == 'call') return Icons.phone_in_talk;
      if (type == 'deal') return Icons.thumb_up;
      if (type == 'whatsapp') return FontAwesomeIcons.whatsapp;
      if (type == 'tgram') return FontAwesomeIcons.telegram;
      if (type == 'gmeet') return FontAwesomeIcons.google;
      if (type == 'zoom') return FontAwesomeIcons.video;
      return Icons.hourglass_bottom;
    }

    Widget iconBody(String type) {
      if (type == 'whatsapp' ||
          type == 'tgram' ||
          type == 'gmeet' ||
          type == 'zoom') {
        return FaIcon(
          setIcon(type),
          size: 16.0,
          color: Colors.grey,
        );
      } else {
        return Icon(
          setIcon(type),
          size: 16.0,
          color: Colors.grey,
        );
      }
    }

    return Container(
      margin: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 7.5, top: 7.5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 0.7), //(x,y)
            blurRadius: 1.0,
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                // Icon(
                //   Icons.email,
                //   size: 16.0,
                //   color: Colors.grey,
                // ),
                iconBody(activity.type != null ? activity.type : ''),
                SizedBox(
                  width: 10.0,
                ),
                Text(
                  activity.type != null ? activity.type : '',
                  style: TextStyle(
                    color: kPrimaryColor,
                    fontSize: 12.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                Icon(
                  Icons.keyboard_arrow_down,
                  size: 16.0,
                  color: Colors.grey,
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            // Text(
            //   'Justin Rhyss',
            //   style: TextStyle(
            //     fontSize: kFontSizeLarge,
            //     fontWeight: FontWeight.w500,
            //   ),
            // ),
            SizedBox(
              height: 5.0,
            ),
            Text(
              activity.activityBody != null ? activity.activityBody : '',
              textAlign: TextAlign.justify,
              style: TextStyle(
                fontSize: kFontSizeLarge,
                color: Colors.black54,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
