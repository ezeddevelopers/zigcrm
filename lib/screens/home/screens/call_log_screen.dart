import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/logs.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/screens/home/components/green_button.dart';
import 'package:zigcrm/screens/home/screens/task_screen.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';
import 'package:zigcrm/utilities/validator.dart';

class CallLog extends StatefulWidget {
  final int customerId;
  CallLog(this.customerId);

  @override
  _CallLogState createState() => _CallLogState();
}

class _CallLogState extends State<CallLog> {
  String outcomeVal = 'call connected';
  String notes;
  bool _loader = false;
  bool followUp = false;
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0.0,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            'LOG CALL',
            style: TextStyle(
                fontSize: kFontSizeBig,
                fontWeight: FontWeight.w300,
                letterSpacing: 0.5),
          ),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ClipPath(
                    clipper: SecondaryClipper(),
                    child: Container(
                      height: size.height * 0.15,
                      color: kPrimaryColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(
                      'CALL OUTCOME',
                      style: TextStyle(
                        fontSize: 31.0,
                        color: Colors.grey,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Row(
                      children: [
                        Radio(
                          value: 'call connected',
                          groupValue: outcomeVal,
                          onChanged: (val) {
                            setState(() {
                              outcomeVal = val;
                            });
                          },
                        ),
                        Text(
                          'Call Connected',
                          style: TextStyle(
                            fontSize: 23.0,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Row(
                      children: [
                        Radio(
                          value: 'busy',
                          groupValue: outcomeVal,
                          onChanged: (val) {
                            setState(() {
                              outcomeVal = val;
                            });
                          },
                        ),
                        Text(
                          'Busy',
                          style: TextStyle(
                            fontSize: 23.0,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Row(
                      children: [
                        Radio(
                          value: 'disconnected',
                          groupValue: outcomeVal,
                          onChanged: (val) {
                            setState(() {
                              outcomeVal = val;
                            });
                          },
                        ),
                        Text(
                          'Disconnected',
                          style: TextStyle(
                            fontSize: 23.0,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Row(
                      children: [
                        Radio(
                          value: 'not in reach',
                          groupValue: outcomeVal,
                          onChanged: (val) {
                            setState(() {
                              outcomeVal = val;
                            });
                          },
                        ),
                        Text(
                          'Not in Reach',
                          style: TextStyle(
                            fontSize: 23.0,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.only(
                  //     left: 20.0,
                  //     right: 20.0,
                  //     top: 30.0,
                  //   ),
                  //   child: Text('Make Notes'),
                  // ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10.0),
                    child: TextFormField(
                      style: TextStyle(
                        fontSize: kFontSizeInputField,
                      ),
                      decoration: InputDecoration(
                        hintText: 'Make Notes',
                        labelText: 'Make Notes',
                        labelStyle: TextStyle(
                          fontSize: kFontSizeLarge,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      validator: (val) => InputValidator().notes(val),
                      onChanged: (val) {
                        setState(() {
                          notes = val;
                        });
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 10.0,
                      vertical: 10.0,
                    ),
                    child: Row(
                      children: [
                        Checkbox(
                          value: followUp,
                          onChanged: (val) {
                            setState(() {
                              followUp = !followUp;
                            });
                          },
                        ),
                        Text(
                          'Create a follow-up task',
                          style: TextStyle(
                            fontSize: 20.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                  GreenButton(
                    label: 'LOG CALL',
                    loader: _loader,
                    onPress: () async {
                      void success(String msg) {
                        setState(() {
                          _loader = false;
                        });
                        Fluttertoast.showToast(
                          msg: msg,
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.black54,
                          textColor: Colors.white,
                          fontSize: 12.0,
                        );
                        if (followUp) {
                          Navigator.pop(context);
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                            return TaskScreen(widget.customerId);
                          }));
                        } else {
                          Navigator.pop(context);
                        }
                      }

                      void failed(String msg) {
                        setState(() {
                          _loader = false;
                        });
                        Fluttertoast.showToast(
                          msg: msg,
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.BOTTOM,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.black54,
                          textColor: Colors.white,
                          fontSize: 12.0,
                        );
                      }

                      if (_formKey.currentState.validate()) {
                        setState(() {
                          _loader = true;
                        });
                        final token = await LocalStorage().getToken();
                        final data = CallFeedbackLog(
                            apiToken: token,
                            callOutcome: outcomeVal,
                            callDescription: notes,
                            customerId: widget.customerId);
                        Provider.of<Store>(context, listen: false)
                            .createCallFeedbackLog(data, success, failed);
                      }
                    },
                  )
                ],
              ),
            ),
          ),
        ));
  }
}

class SecondaryClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height - 50);
    var firstEndPoint = new Offset(size.width / 2.25, size.height - 30.0);
    var firstControlPoint = new Offset(size.width / 4, size.height - 60);
    var secondEndPoint = new Offset(size.width, size.height - 30.0);
    var secondControlPoint =
        new Offset(size.width - (size.width / 3.25), size.height);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
