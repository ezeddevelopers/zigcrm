import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/task.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/screens/home/components/home_layout.dart';
import 'package:zigcrm/screens/home/components/header_round_item.dart';
import 'package:zigcrm/screens/home/components/green_button.dart';
import 'package:date_time_picker/date_time_picker.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';
import 'package:zigcrm/utilities/validator.dart';

class TaskScreen extends StatefulWidget {
  final int customerId;
  TaskScreen(this.customerId);
  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  final _formKey = GlobalKey<FormState>();
  String noteVal;
  DateTime dateVal;
  String timeVal;
  String type = 'Call';
  int staffId;
  String staffName;

  @override
  void initState() {
    Provider.of<Store>(context, listen: false).fetchStaff();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return HomeLayout(
      headerTitle: 'ADD TASK',
      headerItem: HeaderRoundIcon(
        icon: Icons.enhanced_encryption,
      ),
      body: Form(
        key: _formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              child: TextFormField(
                keyboardType: TextInputType.multiline,
                minLines: 1, //Normal textInputField will be displayed
                maxLines: 4,
                style: TextStyle(
                  fontSize: kFontSizeInputField,
                ),
                onChanged: (val) {
                  noteVal = val;
                },
                validator: (value) => InputValidator().notes(value),
                onSaved: (val) => noteVal = val,
                decoration: InputDecoration(
                    hintText: 'Add notes..',
                    hintStyle: TextStyle(
                      color: Colors.black,
                      fontSize: kFontSizeLarge,
                    ),
                    labelText: 'Add Notes',
                    labelStyle: TextStyle(
                      fontSize: kFontSizeLarge + 2,
                      fontWeight: FontWeight.w500,
                      color: kPrimaryColor,
                    )),
              ),
            ),
            Consumer<Store>(
              builder: (context, data, child) {
                final itemData = data.staffs;
                List staffData = [];
                for (int i = 0; i < itemData.data.length; i++) {
                  staffData.add({
                    "display": itemData.data[i].userName,
                    "value": itemData.data[i].userId,
                  });
                }
                return Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10.0),
                  child: DropDownFormField(
                    titleText: 'Assing Staff',
                    hintText: 'Assign Staff',
                    value: staffId,
                    onSaved: (value) {
                      setState(() {
                        staffId = value;
                      });
                    },
                    onChanged: (value) {
                      setState(() {
                        staffId = value;
                      });
                    },
                    dataSource: staffData,
                    textField: 'display',
                    valueField: 'value',
                  ),
                );
              },
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              child: DropDownFormField(
                titleText: 'Type',
                hintText: 'Task Type',
                value: type,
                onSaved: (value) {
                  setState(() {
                    type = value;
                  });
                },
                onChanged: (value) {
                  setState(() {
                    type = value;
                  });
                },
                dataSource: [
                  {
                    "display": "Call",
                    "value": "Call",
                  },
                  {
                    "display": "Email",
                    "value": "Email",
                  },
                  {
                    "display": "To-do",
                    "value": "To-do",
                  },
                ],
                textField: 'display',
                valueField: 'value',
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              child: DateTimePicker(
                type: DateTimePickerType.dateTime,
                firstDate: DateTime(2000),
                lastDate: DateTime(2100),
                dateLabelText: 'Date',
                onChanged: (val) => dateVal = DateTime.parse(val),
                validator: (val) {
                  return null;
                },
                onSaved: (val) => dateVal = DateTime.parse(val),
              ),
            ),
            GreenButton(
              label: 'CREATE TASK',
              onPress: () async {
                void success(String msg) {
                  Fluttertoast.showToast(
                      msg: msg,
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.black54,
                      textColor: Colors.white,
                      fontSize: 12.0);
                  Navigator.pop(context);
                }

                void error(String msg) {
                  Fluttertoast.showToast(
                      msg: msg,
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.black54,
                      textColor: Colors.white,
                      fontSize: 12.0);
                }

                if (_formKey.currentState.validate()) {
                  final token = await LocalStorage().getToken();
                  final data = Task(
                    apiToken: token,
                    custId: widget.customerId,
                    taskDueDate: dateVal,
                    taskDueTime:
                        '${dateVal.hour.toString()}:${dateVal.minute.toString()}:${dateVal.second.toString()}',
                    taskName: noteVal,
                    taskType: type,
                    taskAssignedTo: staffId,
                  );
                  Provider.of<Store>(context, listen: false)
                      .createTask(data, success, error);
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
