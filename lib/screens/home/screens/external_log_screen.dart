import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/logs.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/screens/home/components/home_layout.dart';
import 'package:zigcrm/screens/home/components/header_round_item.dart';
import 'package:zigcrm/screens/home/components/green_button.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';
import 'package:zigcrm/utilities/validator.dart';

class ExternalLogs extends StatefulWidget {
  final int customerId;
  ExternalLogs(this.customerId);

  @override
  _ExternalLogsState createState() => _ExternalLogsState();
}

class _ExternalLogsState extends State<ExternalLogs> {
  String noteVal = '';
  String type = 'call';
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return HomeLayout(
      headerTitle: 'LOG ACTIVITY',
      headerItem: HeaderRoundIcon(
        icon: FontAwesomeIcons.userNurse,
        iconType: 'fa',
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // DetailedPageHeader(),
          Padding(
            padding: const EdgeInsets.fromLTRB(15.0, 40.0, 15.0, 15.0),
            child: Text(
              'WHAT ACTIVITY',
              style: TextStyle(
                fontSize: 23.0,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Row(
            children: [
              Radio(
                value: 'call',
                groupValue: type,
                onChanged: (val) {
                  setState(() {
                    type = val;
                  });
                },
              ),
              Text(
                'PHONE',
                style: TextStyle(
                  fontSize: kFontSizeInputField,
                ),
              ),
              Radio(
                value: 'email',
                groupValue: type,
                onChanged: (val) {
                  setState(() {
                    type = val;
                  });
                },
              ),
              Text(
                'EMAIL',
                style: TextStyle(
                  fontSize: kFontSizeInputField,
                ),
              ),
              Radio(
                value: 'sms',
                groupValue: type,
                onChanged: (val) {
                  setState(() {
                    type = val;
                  });
                },
              ),
              Text(
                'SMS',
                style: TextStyle(
                  fontSize: 12.0,
                ),
              ),
              Radio(
                value: 'whatsapp',
                groupValue: type,
                onChanged: (val) {
                  setState(() {
                    type = val;
                  });
                },
              ),
              Text('WHATSAPP'),
            ],
          ),
          Row(
            children: [
              Radio(
                value: 'tgram',
                groupValue: type,
                onChanged: (val) {
                  setState(() {
                    type = val;
                  });
                },
              ),
              Text(
                'TGRAM',
                style: TextStyle(
                  fontSize: kFontSizeInputField,
                ),
              ),
              Radio(
                value: 'gmeet',
                groupValue: type,
                onChanged: (val) {
                  setState(() {
                    type = val;
                  });
                },
              ),
              Text(
                'GMEET',
                style: TextStyle(
                  fontSize: kFontSizeInputField,
                ),
              ),
              Radio(
                value: 'zoom',
                groupValue: type,
                onChanged: (val) {
                  setState(() {
                    type = val;
                  });
                },
              ),
              Text(
                'ZOOM',
                style: TextStyle(
                  fontSize: kFontSizeInputField,
                ),
              ),
              // Radio(
              //   value: 'skype',
              //   groupValue: type,
              //   onChanged: (val) {
              //     setState(() {
              //       type = val;
              //     });
              //   },
              // ),
              // Text(
              //   'SKYPE',
              //   style: TextStyle(
              //     fontSize: kFontSizeInputField,
              //   ),
              // ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: TextField(
              keyboardType: TextInputType.multiline,
              minLines: 1, //Normal textInputField will be displayed
              maxLines: 4,
              style: TextStyle(
                fontSize: kFontSizeInputField,
              ),
              onChanged: (val) {
                noteVal = val;
              },
              // validator: (value) => InputValidator().notes(value),
              // onSaved: (val) => noteVal = val,
              decoration: InputDecoration(
                  hintText: 'Add notes..',
                  hintStyle: TextStyle(
                    color: Colors.black,
                    fontSize: kFontSizeLarge,
                  ),
                  labelText: 'Add Notes',
                  labelStyle: TextStyle(
                    fontSize: kFontSizeLarge + 2,
                    fontWeight: FontWeight.w500,
                    color: kPrimaryColor,
                  )),
            ),
          ),
          GreenButton(
            label: 'CREATE  LOG',
            onPress: () async {
              void success(String msg) {
                Fluttertoast.showToast(
                    msg: msg,
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.black54,
                    textColor: Colors.white,
                    fontSize: 12.0);
                Navigator.pop(context);
              }

              void error(String msg) {
                Fluttertoast.showToast(
                    msg: msg,
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.black54,
                    textColor: Colors.white,
                    fontSize: 12.0);
              }

              if (noteVal != '') {
                final token = await LocalStorage().getToken();
                final data = ExternalLog(
                  apiToken: token,
                  customerId: widget.customerId,
                  logText: noteVal,
                  logType: type,
                );
                Provider.of<Store>(context, listen: false)
                    .createExternalLog(data, success, error);
              } else {
                Fluttertoast.showToast(
                    msg: 'Activity Note required',
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIosWeb: 1,
                    backgroundColor: Colors.black54,
                    textColor: Colors.white,
                    fontSize: 12.0);
              }
            },
          )
        ],
      ),
    );
  }
}
