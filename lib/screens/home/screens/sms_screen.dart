import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/customer.dart';
import 'package:zigcrm/modal/template.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:zigcrm/utilities/local_storage.dart';

class SMSScreen extends StatefulWidget {
  final Customer customer;
  SMSScreen({this.customer});

  @override
  _SMSScreenState createState() => _SMSScreenState();
}

class _SMSScreenState extends State<SMSScreen> {
  Template selectedTemplate;
  TextEditingController _controllerMessage = TextEditingController();

  @override
  void initState() {
    super.initState();
    Provider.of<Store>(context, listen: false).fetchTemplates('sms_templates');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kDarkGreen,
        elevation: 0.0,
        title: Text(
          widget.customer.customerPhone != null
              ? widget.customer.customerPhone
              : '',
          style: TextStyle(
            fontSize: kFontSizeBig,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: FaIcon(
              FontAwesomeIcons.solidUserCircle,
              color: Colors.white,
              size: 30.0,
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Consumer<Store>(
                builder: (context, data, child) {
                  final itemData = data.templateSms;

                  if (itemData.loading) {
                    return Expanded(
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    );
                  } else if (itemData.error) {
                    return Expanded(
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(itemData.status),
                            FlatButton(
                              color: kPrimaryColor,
                              onPressed: () {
                                data.templateSmsLoading(true);
                                data.fetchTemplates('sms_templates');
                              },
                              child: Text(
                                'Retry',
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }

                  return Expanded(
                    child: ListView.builder(
                      itemCount: itemData.data.length,
                      itemBuilder: (context, index) {
                        final templateData = itemData.data[index];
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              selectedTemplate = templateData;
                              _controllerMessage.text =
                                  templateData.templateBody;
                            });
                          },
                          child: Container(
                            margin: EdgeInsets.symmetric(vertical: 10.0),
                            decoration: BoxDecoration(
                              // color: Colors.grey.shade300,
                              color: itemData.data.contains(selectedTemplate)
                                  ? Colors.grey.shade500
                                  : Colors.grey.shade300,
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            padding: EdgeInsets.symmetric(
                              vertical: 20.0,
                              horizontal: 10.0,
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  templateData.templateName,
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                Text(
                                  templateData.templateBody,
                                  style: TextStyle(
                                    fontSize: 14.0,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black,
                            width: 0.5,
                          ),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: TextField(
                            controller: _controllerMessage,
                            minLines: 1,
                            maxLines: 8,
                            style: TextStyle(
                              fontSize: kFontSizeInputField,
                            ),
                            keyboardType: TextInputType.multiline,
                            decoration: InputDecoration(
                              hintText: 'Message..',
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: IconButton(
                          icon: Icon(
                            Icons.send,
                            size: 30.0,
                          ),
                          onPressed: () async {
                            if (_controllerMessage.text.isNotEmpty) {
                              void success(String msg) {
                                Fluttertoast.showToast(
                                  msg: msg,
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.black54,
                                  textColor: Colors.white,
                                  fontSize: 12.0,
                                );
                                Navigator.pop(context);
                              }

                              void failed(String msg) {
                                Fluttertoast.showToast(
                                  msg: msg,
                                  toastLength: Toast.LENGTH_SHORT,
                                  gravity: ToastGravity.BOTTOM,
                                  timeInSecForIosWeb: 1,
                                  backgroundColor: Colors.black54,
                                  textColor: Colors.white,
                                  fontSize: 12.0,
                                );
                              }

                              final token = await LocalStorage().getToken();
                              final data = SendSmsModal(
                                  token: token,
                                  smsBody: _controllerMessage.text,
                                  customerId: widget.customer.customerId);
                              Provider.of<Store>(context, listen: false)
                                  .sendSms(data, success, failed);
                            } else {
                              Fluttertoast.showToast(
                                msg: 'No message',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIosWeb: 1,
                                backgroundColor: Colors.black54,
                                textColor: Colors.white,
                                fontSize: 12.0,
                              );
                            }
                          },
                        ))
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
