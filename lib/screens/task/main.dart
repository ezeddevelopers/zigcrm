import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/task.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';

class TaskApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Provider.of<Store>(context, listen: false).getAllTasks();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0.0,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Stack(
              children: [
                Container(
                  height: size.height * 0.15,
                  width: size.width,
                  color: kPrimaryColor,
                ),
                Positioned(
                    bottom: 0,
                    child: Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text(
                          'VIEW TASKS',
                          style: TextStyle(
                            fontSize: kFontSizeTitle,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ))),
              ],
            ),
            Consumer<Store>(
              builder: (context, data, child) {
                final itemData = data.allTasks;
                if (itemData.loading) {
                  return Expanded(
                    child: Center(
                      child: SizedBox(
                        child: CircularProgressIndicator(
                          strokeWidth: 2.0,
                          valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                        ),
                        height: 20.0,
                        width: 20.0,
                      ),
                    ),
                  );
                } else if (itemData.error) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(itemData.status),
                      FlatButton(
                        color: kPrimaryColor,
                        onPressed: () {
                          data.allTaskLoading(true);
                          data.getAllTasks();
                        },
                        child: Text(
                          'Retry',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  );
                } else {
                  return Expanded(
                    child: ListView.builder(
                        itemCount: itemData.data.length,
                        itemBuilder: (context, index) {
                          final singleData = itemData.data[index];
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10.0),
                            child: ListTile(
                              leading: CircleAvatar(
                                backgroundColor: kLightGrey,
                                radius: 24.0,
                              ),
                              title: Text(
                                singleData.taskName.toUpperCase(),
                                style: TextStyle(
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              trailing: Checkbox(
                                value: singleData.taskCompleted,
                                onChanged: (val) async {
                                  void success(String msg) {
                                    Fluttertoast.showToast(
                                      msg: msg,
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.black54,
                                      textColor: Colors.white,
                                      fontSize: 12.0,
                                    );
                                  }

                                  void error(String msg) {
                                    Fluttertoast.showToast(
                                      msg: msg,
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.black54,
                                      textColor: Colors.white,
                                      fontSize: 12.0,
                                    );
                                  }

                                  final token = await LocalStorage().getToken();
                                  final data = TaskUpdation(
                                    apiToken: token,
                                    taskId: singleData.taskId,
                                    taskStatus: !singleData.taskCompleted,
                                  );
                                  Provider.of<Store>(context, listen: false)
                                      .changeTaskStatus(data, success, error);
                                },
                              ),
                            ),
                          );
                        }),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
