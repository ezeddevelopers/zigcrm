import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/user.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';
import 'package:zigcrm/utilities/validator.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    String _username;
    String _password;
    Size size = MediaQuery.of(context).size;
    InputValidator inputValidator = new InputValidator();
    final inputNode = FocusScope.of(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: KeyboardAvoider(
            autoScroll: true,
            child: Consumer<Store>(
              builder: (context, loginData, child) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      color: kPrimaryColor,
                      height: size.height * 0.1,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 40.0, bottom: 40.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Image.asset('images/logo.png'),
                      ),
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 20.0),
                            child: Text(
                              'Email',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: kPrimaryColor,
                                fontSize: kFontSizeMedium,
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 15.0, right: 15.0),
                            child: TextFormField(
                              keyboardType: TextInputType.emailAddress,
                              style: TextStyle(
                                fontSize: kFontSizeLarge,
                              ),
                              decoration: InputDecoration(
                                hintText: 'Enter your Email',
                              ),
                              validator: (value) => inputValidator.email(value),
                              onSaved: (username) => _username = username,
                              onEditingComplete: () => inputNode.nextFocus(),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0, right: 15.0, top: 25.0),
                            child: Text(
                              'Password',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: kPrimaryColor,
                                fontSize: kFontSizeMedium,
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 15.0, right: 15.0),
                            child: TextFormField(
                              obscureText: true,
                              style: TextStyle(
                                fontSize: kFontSizeLarge,
                              ),
                              decoration: InputDecoration(
                                hintText: 'Enter your Password',
                              ),
                              validator: (value) =>
                                  inputValidator.password(value),
                              onSaved: (password) => _password = password,
                            ),
                          ),
                          Visibility(
                            visible: loginData.login.error,
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 15.0, right: 15.0, top: 10.0),
                              child: Text(
                                loginData.login.status,
                                style: TextStyle(color: Colors.red),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 15.0,
                                right: 15.0,
                                top: 25.0,
                                bottom: 20.0),
                            child: GestureDetector(
                              onTap: () async {
                                inputNode.unfocus();
                                void loginSuccess(String token) {
                                  Fluttertoast.showToast(
                                      msg: "Logging Successful",
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.black54,
                                      textColor: Colors.white,
                                      fontSize: 12.0);
                                  LocalStorage().storeToken(token);
                                  Navigator.pushNamedAndRemoveUntil(
                                      context, '/appStack', (route) => false);
                                }

                                if (_formKey.currentState.validate()) {
                                  _formKey.currentState.save();
                                  final loginCredentials = LoginPostModal(
                                      username: _username, password: _password);
                                  loginData.loginLoader(true);
                                  loginData.loginCheck(
                                      loginCredentials, loginSuccess);
                                }
                              },
                              child: Container(
                                width: double.infinity,
                                height: 50.0,
                                decoration: BoxDecoration(
                                  gradient: kPrimaryGradient,
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                child: Center(
                                  child: loginData.login.loading
                                      ? SizedBox(
                                          child: CircularProgressIndicator(
                                            strokeWidth: 2.0,
                                            valueColor: AlwaysStoppedAnimation(
                                                Colors.white),
                                          ),
                                          height: 20.0,
                                          width: 20.0,
                                        )
                                      : Text(
                                          'LOGIN',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              Navigator.pushNamed(context, '/forgot-password');
                            },
                            child: Align(
                              alignment: Alignment.center,
                              child: Text(
                                'Forgot Password?',
                                style: TextStyle(
                                  color: kPrimaryColor,
                                  fontSize: kFontSizeMedium,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // Padding(
                    //   padding: const EdgeInsets.only(
                    //     left: 15.0,
                    //     right: 15.0,
                    //     top: 50.0,
                    //     bottom: 20.0,
                    //   ),
                    //   child: Consumer<Store>(
                    //     builder: (context, data, child) {
                    //       return GestureDetector(
                    //         onTap: () async {
                    //           // Navigator.pushNamed(context, '/register');
                    //         },
                    //         child: Container(
                    //           width: double.infinity,
                    //           height: 50.0,
                    //           decoration: BoxDecoration(
                    //             color: Color(0xFF3665AC),
                    //             borderRadius: BorderRadius.circular(5.0),
                    //           ),
                    //           child: Center(
                    //             child: Text(
                    //               'CREATE AN ACCOUNT ${data.customers.data.length}',
                    //               style: TextStyle(color: Colors.white),
                    //             ),
                    //           ),
                    //         ),
                    //       );
                    //     },
                    //   ),
                    // ),
                  ],
                );
              },
            )),
      ),
    );
  }
}
