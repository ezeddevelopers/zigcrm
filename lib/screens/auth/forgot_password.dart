import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/user.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/validator.dart';

class AlwaysDisabledFocusNode extends FocusNode {
  final bool status;
  AlwaysDisabledFocusNode(this.status);
  @override
  bool get hasFocus => status;
}

class ResetPassword extends StatefulWidget {
  ResetPassword({Key key}) : super(key: key);

  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPassword> {
  bool _requested = false;
  bool _loader = false;
  String _username = '';
  String _otp = '';
  String _password = '';
  String _errorMsg = '';
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final inputNode = FocusScope.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        title: Text(
          'RESET PASSWORD',
          style: TextStyle(
            fontSize: 20.0,
            letterSpacing: 0.5,
            fontWeight: FontWeight.w400,
          ),
        ),
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              color: kPrimaryColor,
              height: size.height * 0.1,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Center(
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding:
                              const EdgeInsets.only(top: 80.0, bottom: 50.0),
                          child: Align(
                            alignment: Alignment.center,
                            child: Image.asset('images/logo.png'),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 20.0),
                          child: Text(
                            'Email',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: kPrimaryColor,
                              fontSize: kFontSizeMedium,
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.only(left: 15.0, right: 15.0),
                          child: TextFormField(
                            enableInteractiveSelection:
                                _requested ? false : true,
                            focusNode: new AlwaysDisabledFocusNode(
                                _requested ? false : true),
                            keyboardType: TextInputType.emailAddress,
                            style: TextStyle(
                              fontSize: kFontSizeLarge,
                            ),
                            decoration: InputDecoration(
                              hintText: 'Enter your Email',
                            ),
                            validator: (value) => InputValidator().email(value),
                            onSaved: (value) => _username = value,
                            onChanged: (value) => _username = value,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: _requested
                              ? [
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15.0, right: 15.0, top: 20.0),
                                    child: Text(
                                      'OTP',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: kPrimaryColor,
                                        fontSize: kFontSizeMedium,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15.0, right: 15.0),
                                    child: TextFormField(
                                      keyboardType: TextInputType.emailAddress,
                                      style: TextStyle(
                                        fontSize: kFontSizeLarge,
                                      ),
                                      decoration: InputDecoration(
                                        hintText: 'Enter OTP',
                                      ),
                                      validator: (value) =>
                                          InputValidator().otp(value),
                                      onSaved: (value) => _otp = value,
                                      onChanged: (value) => _otp = value,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15.0, right: 15.0, top: 20.0),
                                    child: Text(
                                      'Password',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: kPrimaryColor,
                                        fontSize: kFontSizeMedium,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 15.0, right: 15.0),
                                    child: TextFormField(
                                      keyboardType: TextInputType.emailAddress,
                                      style: TextStyle(
                                        fontSize: kFontSizeLarge,
                                      ),
                                      decoration: InputDecoration(
                                        hintText: 'Enter your Password',
                                      ),
                                      validator: (value) =>
                                          InputValidator().password(value),
                                      onSaved: (value) => _password = value,
                                      onChanged: (value) => _password = value,
                                    ),
                                  ),
                                ]
                              : [
                                  Container(),
                                ],
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 15.0,
                            vertical: 5.0,
                          ),
                          child: Text(
                            _errorMsg,
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 12,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 15.0, right: 15.0, top: 20.0, bottom: 70.0),
                          child: GestureDetector(
                            onTap: () async {
                              inputNode.unfocus();
                              if (_formKey.currentState.validate()) {
                                if (_requested) {
                                  void success(status) {
                                    setState(() {
                                      _loader = false;
                                    });
                                    Fluttertoast.showToast(
                                      msg: status,
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.black54,
                                      textColor: Colors.white,
                                      fontSize: 12.0,
                                    );
                                    Navigator.pop(context);
                                  }

                                  void failed(status) {
                                    setState(() {
                                      _errorMsg = status;
                                    });
                                  }

                                  final data = PasswordReset(
                                    otp: _otp,
                                    username: _username,
                                    password: _password,
                                  );
                                  Provider.of<Store>(context, listen: false)
                                      .passwordReseting(data, success, failed);
                                } else {
                                  setState(() {
                                    _loader = true;
                                  });
                                  void success(status) {
                                    Fluttertoast.showToast(
                                      msg: status,
                                      toastLength: Toast.LENGTH_SHORT,
                                      gravity: ToastGravity.BOTTOM,
                                      timeInSecForIosWeb: 1,
                                      backgroundColor: Colors.black54,
                                      textColor: Colors.white,
                                      fontSize: 12.0,
                                    );
                                    setState(() {
                                      _loader = false;
                                      _requested = true;
                                    });
                                  }

                                  void failed(status) {
                                    setState(() {
                                      _errorMsg = status;
                                      _loader = false;
                                    });
                                  }

                                  final data = PasswordResetRequest(
                                    username: _username,
                                  );

                                  Provider.of<Store>(context, listen: false)
                                      .passwordResetRequest(
                                          data, success, failed);
                                }
                              }
                            },
                            child: Container(
                              width: double.infinity,
                              height: 50.0,
                              decoration: BoxDecoration(
                                gradient: kPrimaryGradient,
                                borderRadius: BorderRadius.circular(5.0),
                              ),
                              child: Center(
                                child: _loader
                                    ? SizedBox(
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2.0,
                                          valueColor: AlwaysStoppedAnimation(
                                              Colors.white),
                                        ),
                                        height: 20.0,
                                        width: 20.0,
                                      )
                                    : Text(
                                        'RESET PASSWORD',
                                        style: TextStyle(color: Colors.white),
                                      ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
