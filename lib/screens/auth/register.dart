import 'package:flutter/material.dart';
import 'package:zigcrm/utilities/constants.dart';

class RegisterScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  height: size.height * 0.15,
                  width: size.width,
                  color: kPrimaryColor,
                ),
                Positioned(
                    bottom: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.import_contacts,
                            color: Colors.white,
                            size: 30.0,
                          ),
                          SizedBox(
                            width: 15.0,
                          ),
                          Text(
                            'REGISTRATION',
                            style: TextStyle(
                              fontSize: kFontSizeTitle,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          )
                        ],
                      ),
                    )
                ),
              ],
            ),

            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 25.0),
              child: Text(
                'Name',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor,
                  fontSize: kFontSizeMedium,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15.0),
              child: TextFormField(
                style: TextStyle(
                  fontSize: kFontSizeLarge,
                ),
                decoration: InputDecoration(
                  hintText: 'Enter your Name',
                ),
                // validator: (value) => inputValidator.password(value),
                // onSaved: (password) => _password = password,
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 25.0),
              child: Text(
                'Email',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor,
                  fontSize: kFontSizeMedium,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15.0),
              child: TextFormField(
                style: TextStyle(
                  fontSize: kFontSizeLarge,
                ),
                decoration: InputDecoration(
                  hintText: 'Enter your Email',
                ),
                // validator: (value) => inputValidator.password(value),
                // onSaved: (password) => _password = password,
              ),
            ),

            Padding(
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 25.0),
              child: Text(
                'Phone',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: kPrimaryColor,
                  fontSize: kFontSizeMedium,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 15.0, right: 15.0),
              child: TextFormField(
                style: TextStyle(
                  fontSize: kFontSizeLarge,
                ),
                decoration: InputDecoration(
                  hintText: 'Enter your Phone',
                ),
                // validator: (value) => inputValidator.password(value),
                // onSaved: (password) => _password = password,
              ),
            ),

            //
            // Padding(
            //   padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 60.0),
            //   child: Text(
            //     'Name',
            //     style: TextStyle(
            //       fontWeight: FontWeight.bold,
            //       color: kPrimaryColor,
            //       fontSize: 18.0,
            //     ),
            //   ),
            // ),
            // Padding(
            //   padding: const EdgeInsets.only(left: 15.0, right: 15.0),
            //   child: TextField(
            //     decoration: InputDecoration(hintText: 'Enter your Name'),
            //   ),
            // ),
            // Padding(
            //   padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 40.0),
            //   child: Text(
            //     'Email',
            //     style: TextStyle(
            //       fontWeight: FontWeight.bold,
            //       color: kPrimaryColor,
            //       fontSize: 18.0,
            //     ),
            //   ),
            // ),
            // Padding(
            //   padding: const EdgeInsets.only(left: 15.0, right: 15.0),
            //   child: TextField(
            //     decoration: InputDecoration(hintText: 'Enter your Email'),
            //   ),
            // ),
            // Padding(
            //   padding: const EdgeInsets.only(left: 15.0, right: 15.0, top: 25.0),
            //   child: Text(
            //     'Password',
            //     style: TextStyle(
            //       fontWeight: FontWeight.bold,
            //       color: kPrimaryColor,
            //       fontSize: 18.0,
            //     ),
            //   ),
            // ),
            // Padding(
            //   padding: const EdgeInsets.only(left: 15.0, right: 15.0),
            //   child: TextField(
            //     decoration: InputDecoration(hintText: 'Enter your Password'),
            //   ),
            // ),
            Padding(
              padding:
              const EdgeInsets.only(left: 15.0, right: 15.0, top: 35.0, bottom: 20.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, '/appStack');
                },
                child: Container(
                  width: double.infinity,
                  height: 50.0,
                  decoration: BoxDecoration(
                    gradient: kPrimaryGradient,
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Center(
                    child: Text(
                      'CREATE ACCOUNT',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushNamed(context, '/login');
              },
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  'Already have an account?',
                  style: TextStyle(
                      color: kPrimaryColor,
                      fontSize: kFontSizeMedium
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}

