import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';

import 'components/change_password_sheet.dart';
import 'components/edit_profile_sheet.dart';

class ProfileApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Provider.of<Store>(context, listen: false).userDataInfo();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0.0,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Stack(
              children: [
                Container(
                  height: size.height * 0.15,
                  width: size.width,
                  color: kPrimaryColor,
                ),
                Positioned(
                    bottom: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.settings,
                            color: Colors.white,
                            size: 30.0,
                          ),
                          SizedBox(
                            width: 15.0,
                          ),
                          Text(
                            'SETTINGS',
                            style: TextStyle(
                              fontSize: kFontSizeTitle,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          )
                        ],
                      ),
                    )),
              ],
            ),
            Column(
              children: [
                ListItem(
                    title: 'EDIT PROFILE',
                    description: 'Edit your Profile',
                    icon: Icons.person,
                    onPress: () {
                      showModalBottomSheet(
                          context: context,
                          isScrollControlled: true,
                          builder: (context) => SingleChildScrollView(
                                child: Consumer<Store>(
                                  builder: (context, data, child) {
                                    final itemData = data.userInfo;
                                    return Container(
                                      padding: EdgeInsets.only(
                                          bottom: MediaQuery.of(context)
                                              .viewInsets
                                              .bottom),
                                      child: EditProfileBottomSheet(
                                        name: itemData.data.name,
                                        email: itemData.data.email,
                                        phone: itemData.data.phone,
                                      ),
                                    );
                                  },
                                ),
                              )
                          // builder: (context) => AddCustomer(),
                          );
                    }),
                ListItem(
                  title: 'CHANGE PASSWORD',
                  description: 'Change Security',
                  icon: Icons.brightness_high,
                  onPress: () {
                    showModalBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        builder: (context) => SingleChildScrollView(
                              child: Container(
                                padding: EdgeInsets.only(
                                    bottom: MediaQuery.of(context)
                                        .viewInsets
                                        .bottom),
                                child: ChangePasswordBottomSheet(),
                              ),
                            )
                        // builder: (context) => AddCustomer(),
                        );
                  },
                ),
                ListItem(
                  title: 'LOGOUT',
                  description: 'Logout your Account',
                  icon: Icons.open_in_browser,
                  onPress: () {
                    LocalStorage().clearStore();
                    Fluttertoast.showToast(
                        msg: 'Logout!',
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black54,
                        textColor: Colors.white,
                        fontSize: 12.0);
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/login', (route) => false);
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class ListItem extends StatelessWidget {
  final IconData icon;
  final String title;
  final String description;
  final Function onPress;
  ListItem({this.icon, this.title, this.description, this.onPress});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 35.0),
      child: FlatButton(
        padding: EdgeInsets.zero,
        onPressed: onPress,
        child: Row(
          children: [
            Icon(
              icon,
              size: 30.0,
              color: Colors.black,
            ),
            SizedBox(
              width: 15.0,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w400,
                    fontSize: 17.0,
                  ),
                ),
                Text(
                  description,
                  style: TextStyle(
                    color: Colors.black38,
                    fontSize: kFontSizeLarge,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
