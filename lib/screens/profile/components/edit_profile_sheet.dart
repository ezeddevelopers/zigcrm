import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/user.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';
import 'package:zigcrm/utilities/validator.dart';

class EditProfileBottomSheet extends StatefulWidget {
  final String name;
  final String email;
  final String phone; // 'add' & 'edit' two types are there!
  EditProfileBottomSheet({
    this.name,
    this.email,
    this.phone,
  });

  @override
  _EditProfileBottomSheetState createState() => _EditProfileBottomSheetState();
}

class _EditProfileBottomSheetState extends State<EditProfileBottomSheet> {
  String name = '';
  String nameError = '';
  String email = '';
  String emailError = '';
  String phone = '';
  String phoneError = '';
  bool loader = false;

  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {
      _controllerName.text = widget.name;
      _controllerEmail.text = widget.email;
      _controllerPhone.text = widget.phone;
      name = widget.name;
      email = widget.email;
      phone = widget.phone;
    });
  }

  @override
  Widget build(BuildContext context) {
    final inputNode = FocusScope.of(context);
    return Container(
      color: Color(0xff757575),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Align(
                  alignment: Alignment.center,
                  child: Text(
                    'EDIT PROFILE',
                    style: TextStyle(
                      fontSize: kFontSizeLarge,
                      color: kLightGrey,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
                child: TextField(
                  controller: _controllerName,
                  onEditingComplete: () {
                    inputNode.nextFocus();
                  },
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  decoration: InputDecoration(
                    hintText: 'FULL NAME',
                    suffixIcon: Icon(Icons.person),
                  ),
                  onChanged: (val) {
                    name = val;
                  },
                  // validator: (value) => InputValidator().name(value),
                  // onSaved: (val) => name = val,
                ),
              ),
              Text(
                nameError,
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 12.0,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
                child: TextField(
                  controller: _controllerEmail,
                  onEditingComplete: () {
                    inputNode.nextFocus();
                  },
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  decoration: InputDecoration(
                    hintText: 'EMAIL',
                    suffixIcon: Icon(Icons.email),
                    focusColor: kButtonGreenColor,
                  ),
                  onChanged: (val) {
                    email = val;
                  },
                ),
              ),
              Text(
                emailError != null ? emailError : '',
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 12.0,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0, bottom: 5.0),
                child: TextField(
                  controller: _controllerPhone,
                  keyboardType: TextInputType.number,
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  decoration: InputDecoration(
                    hintText: 'PHONE NUMBER',
                    suffixIcon: Icon(Icons.phone),
                  ),
                  onChanged: (val) {
                    phone = val;
                  },
                ),
              ),
              Text(
                phoneError != null ? phoneError : '',
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 12.0,
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Align(
                alignment: Alignment.center,
                child: FlatButton(
                  onPressed: () async {
                    void success(String msg) {
                      Fluttertoast.showToast(
                        msg: msg,
                        toastLength: Toast.LENGTH_SHORT,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIosWeb: 1,
                        backgroundColor: Colors.black54,
                        textColor: Colors.white,
                        fontSize: 12.0,
                      );
                      Navigator.pop(context);
                    }

                    void error(String msg) {
                      setState(() {
                        loader = false;
                        phoneError = msg;
                      });
                    }

                    final nameValidation = InputValidator().name(name);
                    final emailValidation = InputValidator().email(email);
                    final phoneValidation = InputValidator().phone(phone);

                    if (nameValidation != null)
                      setState(() {
                        nameError = emailValidation;
                      });
                    else
                      setState(() {
                        nameError = '';
                      });

                    if (emailValidation != null)
                      setState(() {
                        emailError = emailValidation;
                      });
                    else
                      setState(() {
                        emailError = '';
                      });

                    if (phoneValidation != null)
                      setState(() {
                        phoneError = phoneValidation;
                      });
                    else
                      setState(() {
                        phoneError = '';
                      });

                    if (nameValidation == null &&
                        emailValidation == null &&
                        phoneValidation == null) {
                      if (name != widget.name ||
                          email != widget.email ||
                          phone != widget.phone) {
                        setState(() {
                          loader = true;
                        });
                        final token = await LocalStorage().getToken();
                        final data = UpdateUserModal(
                          apiToken: token,
                          name: name,
                          email: email,
                          phone: int.parse(phone),
                        );
                        Provider.of<Store>(context, listen: false)
                            .updateUserInfo(data, success, error);
                      } else {
                        setState(() {
                          phoneError = 'You can\'t update without any change.';
                        });
                      }
                    }
                  },
                  textColor: Colors.white,
                  color: kButtonGreenColor,
                  padding: EdgeInsets.symmetric(
                    horizontal: 50.0,
                    vertical: 15.0,
                  ),
                  child: loader
                      ? SizedBox(
                          child: CircularProgressIndicator(
                            strokeWidth: 2.0,
                            valueColor: AlwaysStoppedAnimation(Colors.white),
                          ),
                          height: 20.0,
                          width: 20.0,
                        )
                      : Text('UPDATE'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class EditProfileBottomSheetz extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff757575),
      child: Container(
        height: 400.0,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'EDIT PROFILE',
                  style: TextStyle(
                      fontSize: kFontSizeLarge,
                      color: kLightGrey,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  decoration: InputDecoration(
                    hintText: 'FULL NAME',
                    suffixIcon: Icon(Icons.person),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  decoration: InputDecoration(
                    hintText: 'EMAIL',
                    suffixIcon: Icon(Icons.email),
                    focusColor: kButtonGreenColor,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  decoration: InputDecoration(
                    hintText: 'PHONE NUMBER',
                    suffixIcon: Icon(Icons.phone),
                  ),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              FlatButton(
                onPressed: () {},
                textColor: Colors.white,
                color: kButtonGreenColor,
                padding: EdgeInsets.symmetric(
                  horizontal: 50.0,
                  vertical: 15.0,
                ),
                child: Text('UPDATE'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
