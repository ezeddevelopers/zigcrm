import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/modal/user.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/utilities/local_storage.dart';

class ChangePasswordBottomSheet extends StatefulWidget {
  ChangePasswordBottomSheet({Key key}) : super(key: key);

  @override
  _ChangePasswordBottomSheetState createState() =>
      _ChangePasswordBottomSheetState();
}

class _ChangePasswordBottomSheetState extends State<ChangePasswordBottomSheet> {
  String errorMsg;
  bool loader = false;
  TextEditingController _controllerPassword = TextEditingController();
  TextEditingController _controllerNewPassword = TextEditingController();
  TextEditingController _controllerConfirmPassword = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final inputNode = FocusScope.of(context);
    return Container(
      color: Color(0xff757575),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  'CHANGE PASSWORD',
                  style: TextStyle(
                      fontSize: kFontSizeBig,
                      color: kLightGrey,
                      fontWeight: FontWeight.w400),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  controller: _controllerPassword,
                  obscureText: true,
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  onEditingComplete: () {
                    inputNode.nextFocus();
                  },
                  decoration: InputDecoration(
                    hintText: 'CURRENT PASSWORD',
                    labelText: 'CURRENT PASSWORD',
                    suffixIcon: Icon(Icons.lock),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  controller: _controllerNewPassword,
                  obscureText: true,
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  onEditingComplete: () {
                    inputNode.nextFocus();
                  },
                  decoration: InputDecoration(
                    hintText: 'NEW PASSWORD',
                    labelText: 'NEW PASSWORD',
                    suffixIcon: Icon(Icons.lock),
                    focusColor: kButtonGreenColor,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                child: TextField(
                  controller: _controllerConfirmPassword,
                  obscureText: true,
                  style: TextStyle(
                    fontSize: kFontSizeInputField,
                  ),
                  onEditingComplete: () {
                    inputNode.nextFocus();
                  },
                  decoration: InputDecoration(
                    hintText: 'CONFIRM PASSWORD',
                    labelText: 'CONFIRM PASSWORD',
                    suffixIcon: Icon(Icons.lock),
                  ),
                ),
              ),
              Text(
                errorMsg != null ? errorMsg : '',
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 12.0,
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              FlatButton(
                onPressed: () async {
                  void success(String msg) {
                    setState(() {
                      loader = false;
                    });
                    Fluttertoast.showToast(
                      msg: msg,
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.BOTTOM,
                      timeInSecForIosWeb: 1,
                      backgroundColor: Colors.black54,
                      textColor: Colors.white,
                      fontSize: 12.0,
                    );
                    Navigator.pop(context);
                  }

                  void error(String msg) {
                    setState(() {
                      loader = false;
                      errorMsg = msg;
                    });
                  }

                  if (_controllerPassword.text.isEmpty) {
                    setState(() {
                      errorMsg = 'Current Password is Required';
                    });
                  } else if (_controllerNewPassword.text.isEmpty) {
                    setState(() {
                      errorMsg = 'New Password is Required';
                    });
                  } else if (_controllerConfirmPassword.text.isEmpty) {
                    setState(() {
                      errorMsg = 'Confirm Password is Required';
                    });
                  } else if (_controllerNewPassword.text !=
                      _controllerConfirmPassword.text) {
                    errorMsg = 'New password & Confirm password should match';
                  } else {
                    setState(() {
                      loader = true;
                      errorMsg = '';
                    });
                    final token = await LocalStorage().getToken();
                    final data = UpdatePasswordCahngeModal(
                      apiToken: token,
                      currentPassword: _controllerPassword.text,
                      newPassword: _controllerNewPassword.text,
                    );
                    Provider.of<Store>(context, listen: false)
                        .updateUserPassword(data, success, error);
                  }
                },
                textColor: Colors.white,
                color: kButtonGreenColor,
                padding: EdgeInsets.symmetric(
                  horizontal: 50.0,
                  vertical: 15.0,
                ),
                child: loader
                    ? SizedBox(
                        child: CircularProgressIndicator(
                          strokeWidth: 2.0,
                          valueColor: AlwaysStoppedAnimation(Colors.white),
                        ),
                        height: 20.0,
                        width: 20.0,
                      )
                    : Text('UPDATE'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
