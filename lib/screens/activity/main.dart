import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';

class ActivityApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Provider.of<Store>(context, listen: false).fetchAllActivity();

    IconData setIcon(String type) {
      if (type == 'notes') return Icons.rate_review;
      if (type == 'tasks') return Icons.enhanced_encryption;
      if (type == 'customer') return Icons.person_add_alt;
      if (type == 'email') return Icons.email;
      if (type == 'sms') return Icons.messenger;
      if (type == 'call') return Icons.phone_in_talk;
      if (type == 'deal') return Icons.thumb_up;
      if (type == 'whatsapp') return FontAwesomeIcons.whatsapp;
      if (type == 'tgram') return FontAwesomeIcons.telegram;
      if (type == 'gmeet') return FontAwesomeIcons.google;
      if (type == 'zoom') return FontAwesomeIcons.video;
      return Icons.hourglass_bottom;
    }

    Widget iconBody(String type) {
      if (type == 'whatsapp' ||
          type == 'tgram' ||
          type == 'gmeet' ||
          type == 'zoom') {
        return FaIcon(
          setIcon(type),
          size: 18.0,
          color: Colors.grey,
        );
      } else {
        return Icon(
          setIcon(type),
          size: 18.0,
          color: Colors.grey,
        );
      }
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: kPrimaryColor,
        elevation: 0.0,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  height: size.height * 0.15,
                  width: size.width,
                  color: kPrimaryColor,
                ),
                Positioned(
                    bottom: 0,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Row(
                        children: [
                          Icon(
                            Icons.dashboard,
                            color: Colors.white,
                            size: 30.0,
                          ),
                          SizedBox(
                            width: 15.0,
                          ),
                          Text(
                            'ACTIVITY',
                            style: TextStyle(
                              fontSize: kFontSizeTitle,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          )
                        ],
                      ),
                    )),
              ],
            ),
            Consumer<Store>(
              builder: (context, data, child) {
                final itemData = data.allActivity;
                if (itemData.loading) {
                  return Expanded(
                    child: Center(
                      child: SizedBox(
                        child: CircularProgressIndicator(
                          strokeWidth: 2.0,
                          valueColor: AlwaysStoppedAnimation(kPrimaryColor),
                        ),
                        height: 20.0,
                        width: 20.0,
                      ),
                    ),
                  );
                } else if (itemData.error) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(itemData.status),
                      FlatButton(
                        color: kPrimaryColor,
                        onPressed: () {
                          data.allActivityLoading(true);
                          data.fetchAllActivity();
                        },
                        child: Text(
                          'Retry',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  );
                } else {
                  return Expanded(
                    child: ListView.builder(
                      itemCount: itemData.data.length,
                      itemBuilder: (context, index) {
                        final singleData = itemData.data[index];
                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 10.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      singleData.type.toUpperCase(),
                                      style: TextStyle(
                                        fontSize: 17.0,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 20.0,
                                    ),
                                    iconBody(singleData.type),
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Text(
                                  singleData.activityBody != null
                                      ? singleData.activityBody
                                      : '',
                                  textAlign: TextAlign.justify,
                                  style: TextStyle(
                                    fontSize: kFontSizeLarge,
                                    color: Colors.black45,
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
