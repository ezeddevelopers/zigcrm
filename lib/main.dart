import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:zigcrm/provider/provider.dart';
import 'package:zigcrm/utilities/constants.dart';
import 'package:zigcrm/screens/home/main.dart';
import 'package:zigcrm/screens/task/main.dart';
import 'package:zigcrm/screens/activity/main.dart';
import 'package:zigcrm/screens/profile/main.dart';
import 'package:zigcrm/screens/auth/login.dart';
import 'package:zigcrm/screens/auth/register.dart';
import 'package:zigcrm/screens/auth/forgot_password.dart';
import 'package:zigcrm/utilities/local_storage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final token = await LocalStorage().getToken();
  runApp(MyApp(token));
}

class MyApp extends StatelessWidget {
  final token;
  MyApp(this.token);
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return ChangeNotifierProvider(
      create: (_) => Store(),
      child: MaterialApp(
        theme: ThemeData.light().copyWith(primaryColor: kPrimaryColor),
        initialRoute: token != null ? '/appStack' : '/login',
        routes: {
          '/login': (context) => LoginScreen(),
          '/register': (context) => RegisterScreen(),
          '/forgot-password': (context) => ResetPassword(),
          '/appStack': (context) => AppStack(),
        },
      ),
    );
  }
}

class AppStack extends StatefulWidget {
  @override
  _AppStackState createState() => _AppStackState();
}

class _AppStackState extends State<AppStack> {
  int _screenIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    HomeApp(),
    HomeApp(),
    TaskApp(),
    ActivityApp(),
    ProfileApp()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions.elementAt(_screenIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.search),
            label: 'Search',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.next_week_outlined),
            label: 'Task',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.bookmark_outlined),
            label: 'Activity',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.person),
            label: 'Profile',
          ),
        ],
        currentIndex: _screenIndex,
        selectedItemColor: kPrimaryColor,
        unselectedItemColor: kLightGrey,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        onTap: (index) {
          setState(() {
            _screenIndex = index;
          });
        },
      ),
    );
  }
}
