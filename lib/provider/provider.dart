import 'package:flutter/foundation.dart';
import 'package:zigcrm/modal/api_response.dart';
import 'package:zigcrm/modal/customer.dart';
import 'package:zigcrm/modal/customer_activity.dart';
import 'package:zigcrm/modal/logs.dart';
import 'package:zigcrm/modal/note.dart';
import 'package:zigcrm/modal/staff.dart';
import 'package:zigcrm/modal/task.dart';
import 'package:zigcrm/modal/template.dart';
import 'package:zigcrm/modal/user.dart';
import 'package:zigcrm/services/activity.dart';
import 'package:zigcrm/services/customers.dart';
import 'package:zigcrm/services/staff.dart';
import 'package:zigcrm/services/tasks.dart';
import 'package:zigcrm/services/user.dart';
import 'package:zigcrm/utilities/local_storage.dart';

class Store extends ChangeNotifier {
  APIResponse _login = APIResponse();
  APIResponse _customers = APIResponse(data: [], loading: true);
  APIResponse _customerActivity = APIResponse(data: [], loading: true);
  APIResponse _templateWhatsapp = APIResponse(data: [], loading: true);
  APIResponse _templateEmail = APIResponse(data: [], loading: true);
  APIResponse _templateSms = APIResponse(data: [], loading: true);
  APIResponse _staffs = APIResponse(data: [], loading: true);
  List<Staff> _selectedStaff = [];
  APIResponse _allTasks = APIResponse(data: [], loading: true);
  APIResponse _allActivity = APIResponse(data: [], loading: true);
  APIResponse _userInfo = APIResponse(loading: true);

  //********************************************** */
  //          A U T H E N T I C A T I O N          */
  //********************************************** */

  APIResponse get login => _login;
  APIResponse get userInfo => _userInfo;

  void loginCheck(loginCredentials, loginSuccess) async {
    _login = await User().login(loginCredentials);
    notifyListeners();
    if (_login.error == false) {
      loginSuccess(login.data);
    }
  }

  void loginLoader(bool status) {
    _login.loading = true;
    notifyListeners();
  }

  void userDataInfo() async {
    final token = await LocalStorage().getToken();
    final data = UserDataFetching(token);
    _userInfo = await User().getUserInfo(data);
    notifyListeners();
  }

  void updateUserInfo(
      UpdateUserModal data, Function success, Function failed) async {
    final response = await User().updateUserInfo(data);
    if (response.error) {
      failed(response.status);
    } else {
      userDataInfo();
      success(response.status);
    }
  }

  void updateUserPassword(
      UpdatePasswordCahngeModal data, Function success, Function failed) async {
    final response = await User().updateUserPassword(data);
    if (response.error) {
      failed(response.status);
    } else {
      success(response.status);
    }
  }

  void passwordResetRequest(
      PasswordResetRequest data, Function success, Function failed) async {
    final response = await User().resetPasswordRequest(data);
    if (response.error) {
      failed(response.status);
    } else {
      success(response.status);
    }
  }

  void passwordReseting(
      PasswordReset data, Function success, Function failed) async {
    final response = await User().resetUserPassword(data);
    if (response.error) {
      failed(response.status);
    } else {
      success(response.status);
    }
  }

  //********************************************** */
  //              C U S T O M E R S                */
  //********************************************** */

  APIResponse get customers => _customers;
  APIResponse get customerActivity => _customerActivity;
  APIResponse get allActivity => _allActivity;

  void fetchCustomers({String query = "", bool loader = true}) async {
    final token = await LocalStorage().getToken();
    final data = CustomerGetRequest(query: query, token: token);
    _customers = await CustomerApi().getCustomers(data);
    notifyListeners();
  }

  void fetchCustomerActivity(int customerId) async {
    final token = await LocalStorage().getToken();
    final data =
        CustomerActivityRequestModal(token: token, customerId: customerId);
    _customerActivity = await CustomerApi().getCustomerActivity(data);
    notifyListeners();
  }

  void fetchAllActivity() async {
    final token = await LocalStorage().getToken();
    final data = TemplateRequest(token: token);
    _allActivity = await ActivityApi().getActivities(data);
    notifyListeners();
  }

  void customerLoading(bool status) {
    _customers.loading = status;
    notifyListeners();
  }

  void allActivityLoading(bool status) {
    _allActivity.loading = status;
    notifyListeners();
  }

  void customerActivityLoading(bool status) {
    _customerActivity.loading = status;
    notifyListeners();
  }

  void createCustomer(
      CreateCustomer data, Function success, Function failed) async {
    final response = await CustomerApi().createCustomer(data);
    if (response.error) {
      failed(response.status);
    } else {
      fetchCustomers();
      success(response.status);
    }
  }

  void updateCustomer(CreateCustomer data, String customerId, Function success,
      Function failed) async {
    final response = await CustomerApi().updateCustomer(data, customerId);
    if (response.error) {
      failed(response.status);
    } else {
      fetchCustomers();
      success(response.status);
    }
  }

  //********************************************** */
  //              T E M P L A T E S                */
  //********************************************** */

  APIResponse get templateWhatsapp => _templateWhatsapp;
  APIResponse get templateEmail => _templateEmail;
  APIResponse get templateSms => _templateSms;

  void fetchTemplates(String type) async {
    final token = await LocalStorage().getToken();
    final data = TemplateRequest(token: token);
    if (type == 'whatsapp_templates') {
      _templateWhatsapp = await CustomerApi().getTemplates(data, type);
    }
    if (type == 'email_templates') {
      _templateEmail = await CustomerApi().getTemplates(data, type);
    }
    if (type == 'sms_templates') {
      _templateSms = await CustomerApi().getTemplates(data, type);
    }
    notifyListeners();
  }

  void templateWhatsappLoading(bool status) {
    _templateWhatsapp.loading = status;
    notifyListeners();
  }

  void templateEmailLoading(bool status) {
    _templateWhatsapp.loading = status;
    notifyListeners();
  }

  void templateSmsLoading(bool status) {
    _templateWhatsapp.loading = status;
    notifyListeners();
  }

  void sendSms(SendSmsModal data, Function success, Function failed) async {
    final response = await CustomerApi().sendSms(data);
    if (response.error) {
      failed(response.status);
    } else {
      fetchCustomerActivity(data.customerId);
      success(response.status);
    }
  }

  void sendEmail(SendEmailModal data, Function success, Function failed) async {
    final response = await CustomerApi().sendEmail(data);
    if (response.error) {
      failed(response.status);
    } else {
      fetchCustomerActivity(data.customerId);
      success(response.status);
    }
  }

  //********************************************** */
  //                 N O T E S                     */
  //********************************************** */

  void createNote(NoteRequest data, Function success, Function failed) async {
    final response = await CustomerApi().createNote(data);
    if (response.error) {
      failed(response.status);
    } else {
      fetchCustomerActivity(data.customerId);
      success(response.status);
    }
  }

  //********************************************** */
  //                 S T A F F S                   */
  //********************************************** */

  APIResponse get staffs => _staffs;
  List get selectedStaff => _selectedStaff;

  void fetchStaff() async {
    final token = await LocalStorage().getToken();
    final data = TemplateRequest(token: token);
    _staffs = await StaffApi().getStaffs(data);
    notifyListeners();
  }

  void staffsLoading(bool status) {
    _staffs.loading = status;
    notifyListeners();
  }

  void selectStaffToList(Staff data) {
    if (_selectedStaff.contains(data)) {
      _selectedStaff.remove(data);
    } else {
      _selectedStaff.add(data);
    }
    notifyListeners();
  }

  void clearSelectedStaffList() {
    _selectedStaff.clear();
    notifyListeners();
  }

  //********************************************** */
  //                 T A S K S                      */
  //********************************************** */

  APIResponse get allTasks => _allTasks;

  void createTask(Task data, Function success, Function failed) async {
    final response = await CustomerApi().createTask(data);
    if (response.error) {
      failed(response.status);
    } else {
      fetchCustomerActivity(data.custId);
      success(response.status);
    }
  }

  void getAllTasks() async {
    final token = await LocalStorage().getToken();
    final data = TemplateRequest(token: token);
    _allTasks = await TasksApi().getTasks(data);
    notifyListeners();
  }

  void changeTaskStatus(
      TaskUpdation data, Function success, Function failed) async {
    final response = await TasksApi().updateTask(data);
    if (response.error) {
      failed(response.status);
    } else {
      getAllTasks();
      success(response.status);
    }
  }

  void allTaskLoading(bool status) {
    _allTasks.loading = status;
    notifyListeners();
  }

  //********************************************** */
  //         E X T E R N A L    L O G              */
  //********************************************** */

  void createExternalLog(
      ExternalLog data, Function success, Function failed) async {
    final response = await CustomerApi().createExternalLog(data);
    if (response.error) {
      failed(response.status);
    } else {
      fetchCustomerActivity(data.customerId);
      success(response.status);
    }
  }

  void createCallFeedbackLog(
      CallFeedbackLog data, Function success, Function failed) async {
    final response = await CustomerApi().createCallFeedbackLog(data);
    if (response.error) {
      failed(response.status);
    } else {
      fetchCustomerActivity(data.customerId);
      success(response.status);
    }
  }
}
