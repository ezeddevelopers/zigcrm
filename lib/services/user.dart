import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:zigcrm/modal/api_response.dart';
import 'package:zigcrm/modal/user.dart';
import 'package:zigcrm/utilities/endpoints.dart';

class User {
  Future<APIResponse> login(LoginPostModal data) async {
    try {
      final http.Response response =
          await http.post(urlLogin, body: data.toJson());
      if (response.statusCode == 200) {
        final token = json.decode(response.body)['data']['api_token'];
        return APIResponse(
            data: token, loading: false, status: 'Login Successful');
      } else {
        final status = json.decode(response.body)['message'];
        String statusMsg = 'Invalid username or password';
        if (status != null) {
          statusMsg = status;
        }
        return APIResponse(
          loading: false,
          error: true,
          status: statusMsg,
        );
      }
    } catch (e) {
      return APIResponse(
        loading: false,
        error: true,
        status: 'Check your connection. Something went wrong',
      );
    }
  }

  Future<APIResponse> getUserInfo(UserDataFetching data) async {
    try {
      final http.Response response =
          await http.post(urlUserInfo, body: data.toJson());
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body)['data'];
        if (responseData.length > 0) {
          final body = jsonEncode(jsonDecode(response.body)['data']['User']);
          return APIResponse(
            data: userFromJson(body),
            loading: false,
            error: false,
            status: 'Data fetching successful',
          );
        } else {
          return APIResponse(
            data: [],
            error: true,
            status: 'No Data Found',
            loading: false,
            retry: true,
          );
        }
      } else
        return APIResponse(
          data: [],
          error: true,
          status: 'Some error occured. Please try again!',
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> updateUserInfo(UpdateUserModal data) async {
    try {
      final http.Response response = await http.put(urlUpdateUserInfo,
          headers: {"Content-Type": "application/json"},
          body: jsonEncode(data.toJson()));
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body)['message'];
        return APIResponse(
          data: responseData,
          loading: false,
          error: false,
          status: responseData,
        );
      } else
        return APIResponse(
          data: [],
          error: true,
          status: 'Some error occured. Please try again!',
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> updateUserPassword(UpdatePasswordCahngeModal data) async {
    try {
      final http.Response response = await http.put(
        urlUpdateUserPassword,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body)['message'];
        return APIResponse(
          data: responseData,
          loading: false,
          error: false,
          status: responseData,
        );
      } else
        return APIResponse(
          data: [],
          error: true,
          status: 'Some error occured. Please try again!',
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> resetPasswordRequest(PasswordResetRequest data) async {
    try {
      final http.Response response = await http.put(
        urlPasswordReset,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      final responseData = jsonDecode(response.body)['message'];
      if (response.statusCode == 200) {
        return APIResponse(
          data: responseData,
          loading: false,
          error: false,
          status: responseData,
        );
      } else
        return APIResponse(
          data: responseData,
          error: true,
          status: responseData,
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> resetUserPassword(PasswordReset data) async {
    try {
      final http.Response response = await http.post(
        urlForgotPasswordChange,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      final responseData = jsonDecode(response.body)['message'];
      if (response.statusCode == 200) {
        return APIResponse(
          data: responseData,
          loading: false,
          error: false,
          status: responseData,
        );
      } else
        return APIResponse(
          data: responseData,
          error: true,
          status: responseData,
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }
}
