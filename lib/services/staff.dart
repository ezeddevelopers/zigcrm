import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:zigcrm/modal/api_response.dart';
import 'package:zigcrm/modal/staff.dart';
import 'package:zigcrm/modal/template.dart';
import 'package:zigcrm/utilities/endpoints.dart';

class StaffApi {
  Future<APIResponse<List<Staff>>> getStaffs(TemplateRequest data) async {
    try {
      final http.Response response =
          await http.get('$urlStaffs${data.params()}');
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body)['data'];
        if (responseData.length > 0) {
          final body = jsonEncode(jsonDecode(response.body)['data']['user']);
          return APIResponse<List<Staff>>(
            data: staffFromJson(body),
            loading: false,
            error: false,
            status: 'Data fetching successful',
          );
        } else {
          return APIResponse<List<Staff>>(
            data: [],
            error: true,
            status: 'No Data Found',
            loading: false,
            retry: true,
          );
        }
      } else
        return APIResponse<List<Staff>>(
          data: [],
          error: true,
          status: 'Some error occured. Please try again!',
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse<List<Staff>>(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }
}
