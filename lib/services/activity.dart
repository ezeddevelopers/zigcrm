import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:zigcrm/modal/api_response.dart';
import 'package:zigcrm/modal/customer_activity.dart';
import 'package:zigcrm/modal/template.dart';
import 'package:zigcrm/utilities/endpoints.dart';

class ActivityApi {
  Future<APIResponse<List<CustomerActivity>>> getActivities(
      TemplateRequest data) async {
    try {
      final http.Response response =
          await http.get('$urlAllActivities${data.params()}');
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body)['data'];
        if (responseData.length > 0) {
          final body =
              jsonEncode(jsonDecode(response.body)['data']['activity']);
          return APIResponse<List<CustomerActivity>>(
            data: customerActivityFromJson(body),
            loading: false,
            error: false,
            status: 'Data fetching successful',
          );
        } else {
          return APIResponse<List<CustomerActivity>>(
            data: [],
            error: true,
            status: 'No Data Found',
            loading: false,
            retry: true,
          );
        }
      } else
        return APIResponse<List<CustomerActivity>>(
          data: [],
          error: true,
          status: 'Some error occured. Please try again!',
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse<List<CustomerActivity>>(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }
}
