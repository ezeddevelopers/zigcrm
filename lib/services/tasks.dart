import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:zigcrm/modal/api_response.dart';
import 'package:zigcrm/modal/task.dart';
import 'package:zigcrm/modal/template.dart';
import 'package:zigcrm/utilities/endpoints.dart';

class TasksApi {
  Future<APIResponse<List<AllTasks>>> getTasks(TemplateRequest data) async {
    try {
      final http.Response response =
          await http.get('$urlAllTasks${data.params()}');
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body)['data'];
        if (responseData.length > 0) {
          final body = jsonEncode(jsonDecode(response.body)['data']['tasks']);
          return APIResponse<List<AllTasks>>(
            data: allTasksFromJson(body),
            loading: false,
            error: false,
            status: 'Data fetching successful',
          );
        } else {
          return APIResponse<List<AllTasks>>(
            data: [],
            error: true,
            status: 'No Data Found',
            loading: false,
            retry: true,
          );
        }
      } else
        return APIResponse<List<AllTasks>>(
          data: [],
          error: true,
          status: 'Some error occured. Please try again!',
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse<List<AllTasks>>(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> updateTask(TaskUpdation data) async {
    try {
      final http.Response response = await http.post(
        urlUpdateTaskStatus,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return APIResponse(
            data: responseData['message'],
            status: responseData['message'],
            loading: false);
      } else {
        return APIResponse(
          data: [],
          error: true,
          status: 'Something went wrong please try again',
          loading: false,
        );
      }
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }
}
