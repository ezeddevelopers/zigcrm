import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:zigcrm/modal/api_response.dart';
import 'package:zigcrm/modal/customer.dart';
import 'package:zigcrm/modal/customer_activity.dart';
import 'package:zigcrm/modal/logs.dart';
import 'package:zigcrm/modal/note.dart';
import 'package:zigcrm/modal/task.dart';
import 'package:zigcrm/modal/template.dart';
import 'package:zigcrm/utilities/endpoints.dart';

class CustomerApi {
  Future<APIResponse<List<Customer>>> getCustomers(
      CustomerGetRequest data) async {
    try {
      final http.Response response = await http.post(
        urlCustomer,
        body: data.toJson(),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body)['data'];
        if (responseData.length > 0) {
          final body =
              jsonEncode(jsonDecode(response.body)['data']['contacts']);
          return APIResponse<List<Customer>>(
              data: customerFromJson(body),
              loading: false,
              status: 'Data fetching successful');
        } else {
          return APIResponse<List<Customer>>(
            data: [],
            error: true,
            status: 'No Data Found',
            loading: false,
          );
        }
      } else
        return APIResponse<List<Customer>>(
          data: [],
          error: true,
          status: 'Some error occured. Please try again!',
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse<List<Customer>>(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse<List<CustomerActivity>>> getCustomerActivity(
      CustomerActivityRequestModal data) async {
    try {
      final http.Response response =
          await http.get('$urlCustomerActivity${data.params()}');
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body)['data'];
        if (responseData.length > 0) {
          final body =
              jsonEncode(jsonDecode(response.body)['data']['activity']);
          return APIResponse<List<CustomerActivity>>(
              data: customerActivityFromJson(body),
              loading: false,
              status: 'Data fetching successful');
        } else {
          return APIResponse<List<CustomerActivity>>(
            data: [],
            error: true,
            status: 'No Data Found',
            loading: false,
            retry: true,
          );
        }
      } else
        return APIResponse<List<CustomerActivity>>(
          data: [],
          error: true,
          status: 'Some error occured. Please try again!',
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse<List<CustomerActivity>>(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse<List<Template>>> getTemplates(
      TemplateRequest data, String type) async {
    try {
      final http.Response response =
          await http.get('$urlCustomerTemplate${data.params()}');
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body)['data'];
        if (responseData.length > 0) {
          final body = jsonEncode(jsonDecode(response.body)['data'][type]);
          print(responseData);
          return APIResponse<List<Template>>(
              data: templateFromJson(body),
              loading: false,
              error: false,
              status: 'Data fetching successful');
        } else {
          return APIResponse<List<Template>>(
            data: [],
            error: true,
            status: 'No Data Found',
            loading: false,
            retry: true,
          );
        }
      } else
        return APIResponse<List<Template>>(
          data: [],
          error: true,
          status: 'Some error occured. Please try again!',
          loading: false,
          retry: true,
        );
    } catch (err) {
      return APIResponse<List<Template>>(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> createNote(NoteRequest data) async {
    try {
      final http.Response response = await http.post(
        urlCreateNote,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return APIResponse(
            data: responseData['message'],
            status: responseData['message'],
            loading: false);
      } else {
        return APIResponse(
          data: [],
          error: true,
          status: 'Something went wrong please try again',
          loading: false,
        );
      }
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> createTask(Task data) async {
    try {
      final http.Response response = await http.post(
        urlCreateTask,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return APIResponse(
            data: responseData['message'],
            status: responseData['message'],
            loading: false);
      } else {
        return APIResponse(
          data: [],
          error: true,
          status: 'Something went wrong please try again',
          loading: false,
        );
      }
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> createExternalLog(ExternalLog data) async {
    try {
      final http.Response response = await http.post(
        urlExternalLog,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return APIResponse(
            data: responseData['message'],
            status: responseData['message'],
            loading: false);
      } else {
        return APIResponse(
          data: [],
          error: true,
          status: 'Something went wrong please try again',
          loading: false,
        );
      }
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> createCallFeedbackLog(CallFeedbackLog data) async {
    try {
      final http.Response response = await http.post(
        urlCallFeedBack,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return APIResponse(
            data: responseData['message'],
            status: responseData['message'],
            loading: false);
      } else {
        return APIResponse(
          data: [],
          error: true,
          status: 'Something went wrong please try again',
          loading: false,
        );
      }
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> sendSms(SendSmsModal data) async {
    try {
      final http.Response response = await http.post(
        urlSendSms,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return APIResponse(
            data: responseData['message'],
            status: responseData['message'],
            loading: false);
      } else {
        return APIResponse(
          data: [],
          error: true,
          status: 'Something went wrong please try again',
          loading: false,
        );
      }
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> sendEmail(SendEmailModal data) async {
    try {
      final http.Response response = await http.post(
        urlSendEmail,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return APIResponse(
            data: responseData['message'],
            status: responseData['message'],
            loading: false);
      } else {
        return APIResponse(
          data: [],
          error: true,
          status: 'Something went wrong please try again',
          loading: false,
        );
      }
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> createCustomer(CreateCustomer data) async {
    try {
      final http.Response response = await http.post(
        urlCreateCustomer,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return APIResponse(
          data: responseData['message'],
          status: responseData['message'],
          loading: false,
        );
      } else {
        return APIResponse(
          data: [],
          error: true,
          status: 'Something went wrong please try again',
          loading: false,
        );
      }
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }

  Future<APIResponse> updateCustomer(
      CreateCustomer data, String customerId) async {
    try {
      final http.Response response = await http.put(
        '$urlUpdateCustomer/$customerId',
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(data.toJson()),
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
        return APIResponse(
          data: responseData['message'],
          status: responseData['message'],
          loading: false,
        );
      } else {
        return APIResponse(
          data: [],
          error: true,
          status: 'Something went wrong please try again',
          loading: false,
        );
      }
    } catch (err) {
      return APIResponse(
        data: [],
        error: true,
        status: 'Check your connection. Please try again!',
        loading: false,
        retry: true,
      );
    }
  }
}
