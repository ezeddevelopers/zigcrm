class APIResponse<T> {
  T data;
  bool error;
  bool retry;
  String status;
  bool loading;
  APIResponse({
    this.data,
    this.status = '',
    this.retry = false,
    this.loading = false,
    this.error = false,
  });
}
