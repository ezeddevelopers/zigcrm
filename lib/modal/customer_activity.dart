import 'dart:convert';

List<CustomerActivity> customerActivityFromJson(String str) =>
    List<CustomerActivity>.from(
        json.decode(str).map((x) => CustomerActivity.fromJson(x)));

class CustomerActivity {
  CustomerActivity({this.type, this.activityId, this.activityBody});

  String type;
  int activityId;
  String activityBody;

  factory CustomerActivity.fromJson(Map<String, dynamic> json) =>
      CustomerActivity(
        type: json["type"],
        activityId: json["activity_id"],
        activityBody: json["activity_body"],
      );
}

class CustomerActivityRequestModal {
  final String token;
  final int customerId;
  CustomerActivityRequestModal({this.token, this.customerId});

  String params() {
    return '${customerId.toString()}?api_token=$token';
  }
}
