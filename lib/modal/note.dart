class NoteRequest {
  final String token;
  final String note;
  final int customerId;
  final List mentions;
  NoteRequest({this.token, this.note, this.customerId, this.mentions});

  Map<String, dynamic> toJson() => {
        "api_token": token,
        "note_body": note,
        "cust_id": customerId,
        "mentions": List<dynamic>.from(mentions.map((x) => x)),
      };
}
