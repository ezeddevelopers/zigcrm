import 'dart:convert';

List<Staff> staffFromJson(String str) =>
    List<Staff>.from(json.decode(str).map((x) => Staff.fromJson(x)));

class Staff {
  Staff({
    this.userId,
    this.userName,
  });

  int userId;
  String userName;

  factory Staff.fromJson(Map<String, dynamic> json) => Staff(
        userId: json["user_id"],
        userName: json["user_name"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "user_name": userName,
      };
}
