import 'dart:convert';

List<AllTasks> allTasksFromJson(String str) =>
    List<AllTasks>.from(json.decode(str).map((x) => AllTasks.fromJson(x)));

class AllTasks {
  AllTasks({
    this.taskId,
    this.taskName,
    this.taskCompleted,
  });

  int taskId;
  String taskName;
  bool taskCompleted;

  factory AllTasks.fromJson(Map<String, dynamic> json) => AllTasks(
        taskId: json["task_id"],
        taskName: json["task_name"],
        taskCompleted: json["task_completed"],
      );

  Map<String, dynamic> toJson() => {
        "task_id": taskId,
        "task_name": taskName,
        "task_completed": taskCompleted,
      };
}

class Task {
  Task({
    this.apiToken,
    this.custId,
    this.taskDueDate,
    this.taskDueTime,
    this.taskName,
    this.taskType,
    this.taskAssignedTo,
  });

  String apiToken;
  int custId;
  DateTime taskDueDate;
  String taskDueTime;
  String taskName;
  String taskType;
  int taskAssignedTo;

  Map<String, dynamic> toJson() => {
        "api_token": apiToken,
        "cust_id": custId,
        "task_due_date":
            "${taskDueDate.year.toString().padLeft(4, '0')}-${taskDueDate.month.toString().padLeft(2, '0')}-${taskDueDate.day.toString().padLeft(2, '0')}",
        "task_due_time": taskDueTime,
        "task_name": taskName,
        "task_type": taskType,
        "task_assigned_to": taskAssignedTo,
      };
}

class TaskUpdation {
  TaskUpdation({
    this.apiToken,
    this.taskId,
    this.taskStatus,
  });

  String apiToken;
  int taskId;
  bool taskStatus;

  Map<String, dynamic> toJson() => {
        "api_token": apiToken,
        "task_id": taskId,
        "task_status": taskStatus,
      };
}
