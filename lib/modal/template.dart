import 'dart:convert';

List<Template> templateFromJson(String str) =>
    List<Template>.from(json.decode(str).map((x) => Template.fromJson(x)));

class Template {
  Template({
    this.templateId,
    this.templateName,
    this.templateBody,
    this.rawTemplateBody,
  });

  int templateId;
  String templateName;
  String templateBody;
  String rawTemplateBody;

  factory Template.fromJson(Map<String, dynamic> json) => Template(
        templateId: json["template_id"],
        templateName: json["template_name"],
        templateBody: json["template_body"],
        rawTemplateBody: json["raw_template_body"],
      );

  Map<String, dynamic> toJson() => {
        "template_id": templateId,
        "template_name": templateName,
        "template_body": templateBody,
        "raw_template_body": rawTemplateBody,
      };
}

class TemplateRequest {
  final String token;
  TemplateRequest({this.token});

  String params() {
    return '?api_token=$token';
  }
}

class SendSmsModal {
  final String token;
  final int customerId;
  final String smsBody;

  SendSmsModal({this.token, this.customerId, this.smsBody});

  Map<String, dynamic> toJson() => {
        "api_token": token,
        "customer_id": customerId,
        "sms_body": smsBody,
      };
}

class SendEmailModal {
  final String token;
  final int customerId;
  final String emailSub;
  final String emailBody;

  SendEmailModal({this.token, this.customerId, this.emailSub, this.emailBody});

  Map<String, dynamic> toJson() => {
        "api_token": token,
        "customer_id": customerId,
        "email_sub": emailSub,
        "email_body": emailBody
      };
}
