class ExternalLog {
  final String apiToken;
  final int customerId;
  final String logText;
  final String logType;

  ExternalLog({
    this.apiToken,
    this.customerId,
    this.logText,
    this.logType,
  });

  Map<String, dynamic> toJson() => {
        "api_token": apiToken,
        "customer_id": customerId,
        "log_text": logText,
        "log_type": logType,
      };
}

class CallFeedbackLog {
  final String apiToken;
  final int customerId;
  final String callOutcome;
  final String callDescription;

  CallFeedbackLog({
    this.apiToken,
    this.customerId,
    this.callOutcome,
    this.callDescription,
  });

  Map<String, dynamic> toJson() => {
        "api_token": apiToken,
        "cust_id": customerId,
        "call_outcome": callOutcome,
        "call_description": callDescription,
      };
}
