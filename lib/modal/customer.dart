import 'dart:convert';

// ***********************************************************
// FILE INDEX
// 1. CUSTOMER LIST MODAL
// 2. CREATE CUSTOMER MODAL
// ***********************************************************

// ***********************************************************
// 1. CUSTOMER LIST MODAL
// ***********************************************************

List<Customer> customerFromJson(String str) =>
    List<Customer>.from(json.decode(str).map((x) => Customer.fromJson(x)));

class Customer {
  Customer({
    this.registrationDate,
    this.customerId,
    this.customerName,
    this.customerEmail,
    this.customerPhone,
    this.customerPhoneCc,
    this.customerIsBusinessMan,
    this.customerStage,
    this.customerWhatsapp,
    this.customerTelegram,
    this.customerIndustry,
    this.customerEmployees,
    this.customerYrsBusiness,
    this.customerAgeCompany,
    this.customerTurnover,
  });

  DateTime registrationDate;
  int customerId;
  String customerName;
  String customerEmail;
  String customerPhone;
  dynamic customerPhoneCc;
  dynamic customerIsBusinessMan;
  String customerStage;
  String customerWhatsapp;
  dynamic customerTelegram;
  dynamic customerIndustry;
  dynamic customerEmployees;
  dynamic customerYrsBusiness;
  dynamic customerAgeCompany;
  dynamic customerTurnover;

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        registrationDate: DateTime.parse(json["registration_date"]),
        customerId: json["customer_id"],
        customerName: json["customer_name"],
        customerEmail: json["customer_email"],
        customerPhone:
            json["customer_phone"] == null ? null : json["customer_phone"],
        customerPhoneCc: json["customer_phone_cc"],
        customerIsBusinessMan: json["customer_is_business_man"],
        customerStage: json["customer_stage"],
        customerWhatsapp: json["customer_whatsapp"],
        customerTelegram: json["customer_telegram"],
        customerIndustry: json["customer_industry"],
        customerEmployees: json["customer_employees"],
        customerYrsBusiness: json["customer_yrs_business"],
        customerAgeCompany: json["customer_age_company"],
        customerTurnover: json["customer_turnover"],
      );

  Map<String, dynamic> toJson() => {
        "registration_date": registrationDate.toIso8601String(),
        "customer_id": customerId,
        "customer_name": customerName,
        "customer_email": customerEmail,
        "customer_phone": customerPhone == null ? null : customerPhone,
        "customer_phone_cc": customerPhoneCc,
        "customer_is_business_man": customerIsBusinessMan,
        "customer_stage": customerStage,
        "customer_whatsapp": customerWhatsapp,
        "customer_telegram": customerTelegram,
        "customer_industry": customerIndustry,
        "customer_employees": customerEmployees,
        "customer_yrs_business": customerYrsBusiness,
        "customer_age_company": customerAgeCompany,
        "customer_turnover": customerTurnover,
      };
}

class CustomerGetRequest {
  final String query;
  final String token;

  CustomerGetRequest({this.query, this.token});

  Map<String, dynamic> toJson() {
    return {
      "api_token": token,
      "filter": "",
      "search": query,
    };
  }
}

// ***********************************************************
// 2. CREATE CUSTOMER MODAL
// ***********************************************************

class CreateCustomer {
  final String apiToken;
  final String name;
  final String email;
  final int phone;

  CreateCustomer({this.apiToken, this.name, this.email, this.phone});

  Map<String, dynamic> toJson() {
    return {
      "api_token": apiToken,
      "customer_name": name,
      "customer_email": email,
      "customer_phone": phone
    };
  }
}
