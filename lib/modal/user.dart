import 'dart:convert';

UserData userFromJson(String str) => UserData.fromJson(json.decode(str));

class UserData {
  UserData({
    this.id,
    this.name,
    this.email,
    this.phone,
  });

  int id;
  String name;
  String email;
  String phone;
  dynamic userImage;

  factory UserData.fromJson(Map<String, dynamic> json) => UserData(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        phone: json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "phone": phone,
      };
}

class UserDataFetching {
  final token;
  UserDataFetching(this.token);
  Map<String, dynamic> toJson() => {
        "api_token": token,
      };
}

class UpdateUserModal {
  final String apiToken;
  final String name;
  final String email;
  final int phone;

  UpdateUserModal({this.apiToken, this.name, this.email, this.phone});

  Map<String, dynamic> toJson() {
    return {
      "api_token": apiToken,
      "name": name,
      "email": email,
      "phone": phone
    };
  }
}

class UpdatePasswordCahngeModal {
  final String apiToken;
  final String currentPassword;
  final String newPassword;

  UpdatePasswordCahngeModal({
    this.apiToken,
    this.currentPassword,
    this.newPassword,
  });

  Map<String, dynamic> toJson() {
    return {
      "api_token": apiToken,
      "current_password": currentPassword,
      "new_password": newPassword,
    };
  }
}

class LoginPostModal {
  final String username;
  final String password;
  LoginPostModal({this.username, this.password});

  Map<String, dynamic> toJson() => {
        "username": username,
        "password": password,
      };
}

class PasswordResetRequest {
  final String username;

  PasswordResetRequest({
    this.username,
  });

  Map<String, dynamic> toJson() {
    return {
      "username": username,
    };
  }
}

class PasswordReset {
  final String username;
  final String otp;
  final String password;

  PasswordReset({this.username, this.otp, this.password});

  Map<String, dynamic> toJson() {
    return {
      "username": username,
      "otp": otp,
      "password": password,
    };
  }
}
